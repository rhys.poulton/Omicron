\documentclass[aps,longbibliography,nofootinbib,notitlepage]{revtex4-1}

\usepackage{verbatim}
\usepackage{color}
\usepackage{epsfig}
\usepackage{float}
\usepackage{multirow}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{tabularx,url,color}
\usepackage{array}
\usepackage{hyperref}
\usepackage[utf8]{inputenc}
\usepackage{hyperref}

\begin{document}
\leftline{Dated: \today}

\title{omicron-x: a search for unmodeled gravitational-wave burst signals}
\author{Florent Robinet}
\affiliation{Universit\'e Paris-Saclay, CNRS/IN2P3, IJCLab, 91405 Orsay, France}
\maketitle

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
\section{The Omicron analysis of gravitational-wave detectors data}
Let us consider the strain data of two gravitational-wave detectors: $d_H(t)$ and $d_L(t)$. These data are processed separately with the Omicron software~\cite{Robinet:2020lbf,omicron-repo}. The data is first whitened by the noise power: $d^{wh}_H(t)$ and $d^{wh}_L(t)$. Then, the $Q$ transform is applied to decompose the data in a three-dimensional parameter space: the time ($\tau$), frequency ($\phi$), and quality factor ($Q$). The $Q$ transform is a modification of the standard short Fourier transform in which the analysis window $w$ is given a variable size:
\begin{equation}
  D_{H,L}(\tau, \phi, Q) = \int_{-\infty}^{+\infty}{ d_{H,L}^{wh}(t) w(t-\tau,\phi,Q) e^{-2i\pi\phi t}dt}.
  \label{eq:qtransform}
\end{equation}
The whitening step offers a statistical interpretation for the $Q$ transform: the resulting transform coefficients, $D_H$ and $D_L$, are used to derive signal-to-noise ratio estimators,
\begin{equation}
  \hat{\rho}^2_{H,L}(\tau,\phi,Q) =
  \left\{
  \begin{array}{ll}
    \left|D_{H,L}(\tau, \phi, Q)\right|^2-2, & \mathrm{if}\ \left|D_{H,L}(\tau, \phi, Q)\right|^2 \ge 2\\
    0, & \mathrm{otherwise}.
  \end{array}
  \right.
  \label{eq:snr}
\end{equation}
The signal-to-noise ratio estimators $\hat{\rho}_H$ and $\hat{\rho}_L$ can be represented as spectrograms in the $(\tau,\phi)$ plane, fixing $Q$. In Fig.~\ref{fig:gw150914}, the Omicron spectrograms of the GW150914 event is represented for both LIGO detectors.
\begin{figure}
  \center
  \includegraphics[width=12cm]{./figures/gw150914_h1.png} \\
  \includegraphics[width=12cm]{./figures/gw150914_l1.png}
  \caption{Omicron spectrograms of GW150914 ($Q=9.2$) as detected by LIGO-hanford (top) and LIGO-Livingston (bottom) detectors.}
  \label{fig:gw150914}
\end{figure}

The signal-to-noise ratio spectrograms $\hat{\rho}_H(\tau,\phi,Q)$ and $\hat{\rho}_L(\tau,\phi,Q)$ are computed for a fixed $Q$ value and over a finite region of time and frequency: $\tau_{min} \le \tau < \tau_{max}$, $\phi_{min} \le \phi < \phi_{max}$. Moreover the time-frequency space is discretized using a tiling structure defined in~\cite{omicron}. The spectrograms are divided in logarithmically-spaced frequency rows centered on $\phi_l$, where $l$ runs from 0 to $N_\phi-1$. Each row is sub-divided into $N_\tau(\phi_l)$ linearly-spaced time bins. In Omicron, the number of time bins $N_\tau(\phi_l)$ is chosen to be a power of two to optimize Fourier transforms. With this structure, the number of time bins $N_\tau(\phi_l)$ is a sub-multiple of $N_\tau(\phi_{l+1})$. As a consequence, the tiling structure can be mapped over a $N_\phi\times N_\tau$ grid where $N_\tau=N_\tau(\phi_{N_\phi-1})$ is the number of time bins in the highest frequency row. In a discrete and finite parameter space, we note the signal-to-noise estimator\footnote{Note that we drop the hat to simplify notations.} $\rho_{H,L}[m][l]$, where $m$ and $l$ are the time and frequency indices respectively.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
\section{The cross-correlation of spectrograms}
The spectrograms of detector $H$ and detector $L$ are cross-correlated along the time direction:
\begin{equation}
  \xi[m][l] = \sum_{m^{\prime}=0}^{(N_\tau-1)} \rho^2_H[m^{\prime}][l] \times \rho^2_L[N_\tau/2-m+m^{\prime}][l].
  \label{eq:xcorr}
\end{equation}
For a given frequency row $l$, the cross-correlation coefficient $\xi$ measures the sum of $\rho^2_H\rho^2_L$ when the $\rho^2_H$ time bins are shifted by $m-N_\tau/2$ bins with respect to those of $\rho^2_L$. This calculation is represented in Fig.~\ref{fig:xcorr}. The coefficient $\xi$ can be seen as a function of the time shift $\delta \tau$ between detector 1 and detector 2:
\begin{equation}
  \delta \tau = \left (m-\frac{N_\tau}{2}\right) \times \frac{\tau_{max}-\tau_{min}}{N_\tau}.
  \label{eq:timeshift}
\end{equation}
We note that, in Eq.~\ref{eq:xcorr}, the index $N_\tau/2-m+m^{\prime}$ can be out-of-range for the time series $\rho_L$. As often when dealing with finite discrete functions, we assume circular periodicity: $(N_\tau/2-m+m^{\prime}) \pmod{N_\tau}$. In the following, to simplify notations, we will not explicitely address out-of-range issues and keep light notations.
\begin{figure}
  \center
  \includegraphics[width=16cm]{./figures/xcorr.pdf}
  \caption{Representation of three cross-correlation coefficients for a given frquency row $l$: $\xi[0][l]$ (left), $\xi[N_\tau/2][l]$ (center), and $\xi[N_\tau-1][l]$. The detector $H$ (blue) time bins are shifted by $\delta t$ with respect to those of detector $L$ (green). The resulting cross-correlation coefficient is represented by the red square. The dashed-line squares indicate the circular periodicity used to compute the cross-correlation.}
  \label{fig:xcorr}
\end{figure}

To optimize the computation of $\xi[m][l]$, the cross-correlation is calculated in the Fourier domain. We define the Fourier transform of a discrete time series $x$ of size $N_\tau$ as:
\begin{equation}
  \tilde{x}[k] = \frac{1}{N_\tau} \sum_{m=0}^{(N_\tau-1)} x[m] e^{-2i\pi km / N_\tau},
  \label{eq:dft}
\end{equation}
where $i=\sqrt{-1}$. The inverse Fourier transform is given by:
\begin{equation}
  x[m] = \sum_{k=0}^{(N_\tau-1)} \tilde{x}[k] \times e^{+2i\pi km / N_\tau}.
  \label{eq:invdft}
\end{equation}

Using the definition of Eq.~\ref{eq:dft}, we write Eq.~\ref{eq:xcorr} in the Fourier domain:
\begin{equation}
  \tilde{\xi}[k][l] = \frac{1}{N_\tau} \sum_{m=0}^{(N_\tau-1)} \sum_{m^{\prime}=0}^{(N_\tau-1)} \rho_H[m^{\prime}][l] \times \rho_L[N_\tau/2-m+m^{\prime}][l] \times e^{-2i\pi km / N_\tau}.
\end{equation}
We substitute $n$ for $N_\tau/2-m+m^{\prime}$ and we get:
\begin{align}
  \tilde{\xi}[k][l] &= \frac{1}{N_\tau} \sum_{m^{\prime}=0}^{(N_\tau-1)} \sum_{n=m^{\prime}-N_\tau/2+1}^{(N_\tau/2+m^{\prime})} \rho_H[m^{\prime}][l] \times \rho_L[n][l] \times e^{-2i\pi k(N_\tau/2-n+m^{\prime}) / N_\tau}, \nonumber \\
  &= \frac{(-1)^k}{N_\tau} \sum_{m^{\prime}=0}^{(N_\tau-1)} \rho_H[m^{\prime}][l] \times e^{-2i\pi km^{\prime} / N_\tau}
  \sum_{n=m^{\prime}-N_\tau/2+1}^{(N_\tau/2+m^{\prime})}  \rho_L[n][l] \times e^{+2i\pi kn / N_\tau}.
  \label{eq:xcorr_theorem}
\end{align}
In Eq.~\ref{eq:xcorr_theorem}, the first sum is the Fourier transform of $\rho_H$. For the second sum, $\rho_L$ and the exponential are $N_\tau$-periodic functions of so that we can apply a shift of $m^{\prime}-N_\tau/2+1$ without changing the result of the sum:
\begin{align}
  \tilde{\xi}[k][l] &= (-1)^k \times \tilde{\rho}_H[k][l] \times \sum_{n=0}^{(N_\tau-1)}  \rho_L[n][l] \times e^{+2i\pi kn / N_\tau} \nonumber \\
  &= (-1)^k \times N_\tau \times \tilde{\rho}_H[k][l] \times \tilde{\rho}_L^*[k][l],
  \label{eq:xcorr_ft}
\end{align}
where $\tilde{\rho}_L^*$ is the complex conjugate of $\tilde{\rho}_L$. Equation~\ref{eq:xcorr_ft} is a variant of the well-known cross-correlation theorem: there is an extra phase of $\pi$ due to the fact that we chose a $N_\tau/2$ shift in Eq.~\ref{eq:xcorr}.

Finally, the cross-correlation coefficients are obtained by performing the inverse Fourier transform of Eq.~\ref{eq:xcorr_ft}:
\begin{equation}
  \xi[m][l] = \sum_{k=0}^{(N_\tau-1)} (-1)^k \times N_\tau \times \tilde{\rho}_H[k][l] \times \tilde{\rho}_L^*[k][l] \times e^{+2i\pi km / N_\tau}.
  \label{eq:xcorr2}
\end{equation}

This function is calculated with 12 seconds of data centered on GW150914 and is represented in Fig.~\ref{fig:gw150914_xi}. The correlation is maximum when the two LIGO spectrograms are aligned: $\delta t = 0$.
\begin{figure}
  \center
  \includegraphics[width=16cm]{./figures/gw150914_xi.png}
  \caption{Cross-correlation function $\xi(\delta t,\phi, Q=8.2)$ calculated with 12-seconds LIGO-Hanford and LIGO-Livingston spectrograms of GW150914.}
  \label{fig:gw150914_xi}
\end{figure}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
\bibliography{references}
\end{document}
