#include "OmicronUtils.h"
#include "Oconfig.h"
#include <TROOT.h>
#include <TKey.h>
#include <TFile.h>
#include <FFT.h>
#include <GwollumPlot.h>
#include <TRandom.h>


using namespace std;

/**
 * @brief Main program.
 */
int main (int argc, char* argv[]){

  const string mapn = "map2";
  const unsigned int overlap = 4;// s
  const double zero_lag_dt = 1.0;// s (+/-)

  if(argc<3) return -1;
  
  string dir1 = (string)argv[1];
  string dir2 = (string)argv[2];
  bool savespectro=false;
  if(argc==4) savespectro=true;
  TH2D *rho1_raw, *rho2_raw, *rho1, *rho2;
  TH1D *xi, *xi_bg;
  TH1D *xi_bg_tot=NULL, *xi_bg_cum_tot=NULL, *xi_0_tot=NULL, *xi_0_cum_tot=NULL;
  unsigned int nt, m_s;
  double window, window_raw;
  double realpart, norm;
  double xi0_max=0;
  double dt;
  string gps1;
  fft *ft_1 = NULL;
  fft *ft_2 = NULL;
  TF1 *fitfn = new TF1("fitfn", "gaus(0)");
  double nsigma;
  
  // get list of root files
  vector<string> file1;
  vector<string> file2;
  if(IsDirectory(dir1)) file1 = Glob((dir1+"/*_OMICRONMAPQ-1*.root").c_str());
  else file1.push_back(dir1);
  if(IsDirectory(dir2)) file2 = Glob((dir2+"/*_OMICRONMAPQ-1*.root").c_str());
  else file2.push_back(dir2);
  string fn2;

  // output file
  TFile *outf = new TFile("x_out.root", "recreate");

  // loop over files in dir1
  for(unsigned int f1 = 0; f1<file1.size(); f1++){

    // get GPS from file name
    gps1=file1[f1].substr(file1[f1].size()-15, 10);

    // get det2 counterpart
    fn2="";
    for(unsigned int f2 = 0; f2<file2.size(); f2++){
      if (file2[f2].find(gps1)!=std::string::npos){
        fn2=file2[f2];
        break;
      }
    }

    // counterpart is required
    if(!fn2.compare("")){
      cerr<<"No counterpart found for "<<file1[f1]<<endl;
      return 1;
    }
    cout<<gps1<<"..."<<endl;

    // get rho1
    TFile *tf1 = new TFile(file1[f1].c_str(), "READ");
    tf1->GetObject(mapn.c_str(), rho1_raw);
    rho1_raw->SetDirectory(0);
    tf1->Close();

    // get rho2
    TFile *tf2 = new TFile(fn2.c_str(), "READ");
    tf2->GetObject(mapn.c_str(), rho2_raw);
    rho2_raw->SetDirectory(0);
    tf2->Close();

    if(rho1_raw == 0){
      cerr<<"rho1_raw == 0"<<endl;
      return 1;
    }
    if(rho2_raw == 0){
      cerr<<"rho2_raw == 0"<<endl;
      return 1;
    }
    
    if(rho1_raw->GetNbinsX()!=rho2_raw->GetNbinsX()){
      cerr<<"rho1_raw->GetNbinsX()!=rho2_raw->GetNbinsX()"<<endl;
      return 1;
    }
    
    if(rho1_raw->GetNbinsY()!=rho2_raw->GetNbinsY()){
      cerr<<"rho1_raw->GetNbinsY()!=rho2_raw->GetNbinsY()"<<endl;
      return 1;
    }

    // conditioning (remove overlap)
    window_raw = rho1_raw->GetXaxis()->GetBinUpEdge(rho1_raw->GetNbinsX()) - rho1_raw->GetXaxis()->GetBinLowEdge(1);
    window = window_raw - (double)overlap;
    nt = (unsigned int)(window/window_raw * (double)rho1_raw->GetNbinsX());
    m_s = (unsigned int)((double)overlap/2.0/window_raw * (double)rho1_raw->GetNbinsX());
    rho1 = new TH2D("rho1", "rho1", nt,
                    rho1_raw->GetXaxis()->GetBinLowEdge(1)+(double)overlap/2.0,
                    rho1_raw->GetXaxis()->GetBinLowEdge(1)+(double)overlap/2.0 + (double)nt*rho1_raw->GetXaxis()->GetBinWidth(1),
                    rho1_raw->GetNbinsY(), rho1_raw->GetYaxis()->GetXbins()->GetArray());
    rho2 = new TH2D("rho2", "rho2", nt,
                    rho2_raw->GetXaxis()->GetBinLowEdge(1)+(double)overlap/2.0,
                    rho2_raw->GetXaxis()->GetBinLowEdge(1)+(double)overlap/2.0 + (double)nt*rho2_raw->GetXaxis()->GetBinWidth(1),
                    rho2_raw->GetNbinsY(), rho2_raw->GetYaxis()->GetXbins()->GetArray());
    for(unsigned int l=0; l<rho1->GetNbinsY(); l++){
      for(unsigned int m=0; m<rho1->GetNbinsX(); m++){
        rho1->SetBinContent(m+1,l+1, rho1_raw->GetBinContent(m_s+m+1,l+1));
        rho2->SetBinContent(m+1,l+1, rho2_raw->GetBinContent(m_s+m+1,l+1));
      }
    }
    rho1->SetStats(0);
    rho2->SetStats(0);
    
    cout<<"\trho map size: "<<rho1->GetNbinsX()<<" x "<<rho1->GetNbinsY()<<" "<<rho1_raw->GetYaxis()->GetXbins()->GetSize()<<endl;
    cout<<"\trho map time range: "<<window<<" s"<<endl;

    // Xi function
    xi = new TH1D(("xi_"+gps1).c_str(), "Cross-correlation", rho1->GetNbinsX(), -window/2.0, window/2.0);
    xi->Reset();
    xi->SetStats(0);
    xi->GetXaxis()->SetTitle("#delta t [s]");
    xi->GetYaxis()->SetTitle("#Xi");

    // FFT allocation (first time only)
    if(ft_1==NULL){
      ft_1 = new fft(rho1->GetNbinsX());
      ft_2 = new fft(rho2->GetNbinsX());
    }
    
    // loop over frequency rows
    for(unsigned int l=0; l<rho1->GetNbinsY(); l++){

      // FT 1
      for(unsigned int m=0; m<rho1->GetNbinsX(); m++){
        ft_1->SetRe_t(m, rho1->GetBinContent(m+1,l+1)/rho1->GetNbinsX());
        ft_1->SetIm_t(m, 0.0);
      }
      ft_1->Forward();
      
      // FT 2
      for(unsigned int m=0; m<rho2->GetNbinsX(); m++){
        ft_2->SetRe_t(m, rho2->GetBinContent(m+1,l+1)/rho2->GetNbinsX());
        ft_2->SetIm_t(m, 0.0);
      }
      ft_2->Forward();
      
      // compute xi
      for(unsigned int k=0; k<rho1->GetNbinsX(); k++){
        
        // norm + phase factors
        if(k%2==1) norm = -rho1->GetNbinsX();
        else       norm = rho1->GetNbinsX();
        
        realpart = ft_1->GetRe_f(k);
        ft_1->SetRe_f(k, norm*(realpart*ft_2->GetRe_f(k) + ft_1->GetIm_f(k)*ft_2->GetIm_f(k)));
        ft_1->SetIm_f(k, norm*(ft_1->GetIm_f(k)*ft_2->GetRe_f(k) - realpart*ft_2->GetIm_f(k)));
      }
      ft_1->Backward();
      
      // integrate xi over frequencies
      for(unsigned int m=0; m<rho1->GetNbinsX(); m++){
        xi->SetBinContent(m+1, xi->GetBinContent(m+1) + ft_1->GetRe_t(m));
      }
      
    }

    // cumulative distribution
    xi_bg = new TH1D(("xi_bg_"+gps1).c_str(), "Background", 1000, xi->GetMinimum(), 1.5*xi->GetMaximum());
    if(xi_bg_tot==NULL) xi_bg_tot = new TH1D("xi_bg_tot", "Background", 1000, xi->GetMinimum(), 1.5*xi->GetMaximum());
    if(xi_0_tot==NULL) xi_0_tot = new TH1D("xi_0_tot", "Zero-lag", 1000, xi->GetMinimum(), 1.5*xi->GetMaximum());

    // background
    for(unsigned int m=0; m<rho1->GetNbinsX(); m++){
      dt = ((double)m-(double)rho1->GetNbinsX()/2.0)*window/(double)rho1->GetNbinsX();
      if(fabs(dt)>zero_lag_dt){
        xi_bg->Fill(xi->GetBinContent(m+1));
        xi_bg_tot->Fill(xi->GetBinContent(m+1));
      }
    }

    // zero-lag
    xi_0_tot->Fill(xi->GetBinContent(rho1->GetNbinsX()/2));
    if(xi->GetBinContent(rho1->GetNbinsX()/2)>xi0_max) xi0_max=xi->GetBinContent(rho1->GetNbinsX()/2);
    cout<<"Xi(0) = "<<xi->GetBinContent(rho1->GetNbinsX()/2)<<" (max = "<<xi0_max<<")"<<endl;

    // Gaussian fit
    xi_bg->Fit(fitfn);
    nsigma = (xi->GetBinContent(rho1->GetNbinsX()/2) - fitfn->GetParameter(1))/fitfn->GetParameter(2);
    if(nsigma>2.0) cout<<">>>>>>>>>>>>>>>>>>> "<<nsigma<<" sigmas"<<endl;
    else cout<<">> "<<nsigma<<" sigmas"<<endl;
    
    outf->cd();
    xi->Write();
    xi_bg->Write();
    if(savespectro){
      rho1->Write(("rho1_"+gps1).c_str());
      rho2->Write(("rho2_"+gps1).c_str());
    }
    
    delete xi;
    delete xi_bg;
    delete rho1_raw;
    delete rho2_raw;
    delete rho1;
    delete rho2;
  }

  // cumulative distribution
  if(xi_bg_tot!=NULL){
    xi_bg_cum_tot = (TH1D*)xi_bg_tot->GetCumulative(kFALSE);
    for(unsigned int m=0; m<xi_bg_cum_tot->GetNbinsX(); m++){
      xi_bg_cum_tot->SetBinError(m+1, sqrt(xi_bg_cum_tot->GetBinContent(m+1)));
    }
    xi_bg_cum_tot->Scale(1.0/(double)xi_bg_tot->GetEntries());
  }
  if(xi_0_tot!=NULL){
    xi_0_cum_tot = (TH1D*)xi_0_tot->GetCumulative(kFALSE);
    for(unsigned int m=0; m<xi_0_cum_tot->GetNbinsX(); m++){
      xi_0_cum_tot->SetBinError(m+1, sqrt(xi_0_cum_tot->GetBinContent(m+1)));
    }
    xi_0_cum_tot->Scale(1.0/(double)xi_0_tot->GetEntries());
  }

  
  outf->cd();
  if(xi_bg_tot!=NULL){
    xi_bg_tot->Write();
    delete xi_bg_tot;
  }
  if(xi_bg_cum_tot!=NULL){
    xi_bg_cum_tot->Write();
    delete xi_bg_cum_tot;
  }
  if(xi_0_tot!=NULL){
    xi_0_tot->Write();
    delete xi_0_tot;
  }
  if(xi_0_cum_tot!=NULL){
    xi_0_cum_tot->Write();
    delete xi_0_cum_tot;
  }
  outf->Close();

  return 0;
}
