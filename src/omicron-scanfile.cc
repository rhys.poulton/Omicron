/**
 * @file 
 * @brief Program to scan and identify corrupted trigger files.
 * @details `omicron-scanfile` is a command line program to analyze a set of omicron trigger files.
 * The program must be given a minimum set of options:
 * @verbatim
omicron-scanfile channel=[channel name] gps-start=[GPS start] gps-end=[GPS end]
 @endverbatim
 * This command scans omicron trigger files for a given channel between 2 GPS times.
 * This command assumes that omicron trigger root files are saved in a standard place pointed by the environement variable `$OMICRON_TRIGGERS`.
 *
 * One can also scan files from a list with:
 * @verbatim
 omicron-scanfile file=[trigger file pattern]
 @endverbatim
 * where `[trigger file pattern]` can contain wild cards. For example: `file="/path1/to/triggers/\*.root /path2/to/triggers/\*.root"`.
 *
 * @snippet this omicron-scanfile-usage 
 *
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#include <TFile.h>
#include "OmicronUtils.h"

using namespace std;

/**
 * @brief Print the program usage message.
 */
void PrintUsage(void){
  //! [omicron-scanfile-usage]
  cerr<<endl;
  cerr<<"Usage:"<<endl;
  cerr<<endl;
  cerr<<"omicron-scanfile channel=[channel name] \\"<<endl;
  cerr<<"                 file=[trigger file pattern] \\"<<endl;
  cerr<<"                 gps-start=[GPS start] \\"<<endl;
  cerr<<"                 gps-end=[GPS end]"<<endl;
  cerr<<endl;
  cerr<<"[channel name]            channel name used to retrieve centralized Omicron triggers"<<endl;
  cerr<<"[trigger file pattern]    file pattern to ROOT trigger files"<<endl;
  cerr<<"[GPS start]               starting GPS time (integer only)"<<endl;
  cerr<<"[GPS end]                 stopping GPS time (integer only)"<<endl;
  cerr<<endl;
  //! [omicron-scanfile-usage]
  return;
}

/**
 * @brief Main program.
 */
int main (int argc, char* argv[]){

  if(argc>1&&!((string)argv[1]).compare("version")){
    return OmicronPrintVersion();
  }

  // number of arguments
  if(argc<2){
    PrintUsage();
    return 1;
  }

  // list of parameters + default
  string chname = "";     // channel name
  string tfile_pat = "";  // trigger file pattern
  unsigned int gps_start = 0; // GPS start
  unsigned int gps_end = 0; // GPS end

  // loop over arguments
  vector <string> sarg;
  for(int a=1; a<argc; a++){
    sarg=SplitString((string)argv[a],'=');
    if(sarg.size()!=2) continue;
    if(!sarg[0].compare("channel"))    chname=(string)sarg[1];
    if(!sarg[0].compare("file"))       tfile_pat=(string)sarg[1];
    if(!sarg[0].compare("gps-start"))  gps_start=stoul(sarg[1].c_str());
    if(!sarg[0].compare("gps-end"))    gps_end=stoul(sarg[1].c_str());
  }

  // centralized trigger files
  if(!tfile_pat.compare("")){
    if(chname.compare("") && (gps_start>0) && (gps_end>0))
      tfile_pat=GetOmicronFilePattern(chname, gps_start, gps_end);// get centralized trigger files
    else{
      cerr<<"Triggers must be specified, either with a channel name and a time range or with a file pattern"<<endl;
      cerr<<"Type omicron-scanfile for help"<<endl;
      return 1;
    }
  }
  stringstream tmpstream;

  // check file pattern
  if(!tfile_pat.compare("")){
    cerr<<"No trigger files"<<endl;
    return 2;
  }

  // list of files
  string ls = "ls -U "+tfile_pat;
  
  // file descriptor
  FILE *in = popen(ls.c_str(), "r");

  // loop over files
  char filename[100000];
  while(fscanf(in,"%s",filename) != EOF){
    
    TFile *tmp = new TFile(filename);
    if( !tmp || tmp->IsZombie() ){
      cout<<"ZOMBIE  "<<filename<<endl;
      continue;
    }
    if( tmp->ReadKeys() ){
      cout<<"OK      "<<filename<<endl;
      tmp->Close();
    }
    else
      cout<<"NOKEYS  "<<filename<<endl;
      
  }
  pclose(in);

  return 0;
}

