/**
 * @file 
 * @brief See Oomicron.h
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#include "Oomicron.h"

ClassImp(Omicron)


////////////////////////////////////////////////////////////////////////////////////
Omicron::Omicron(const string aOptionFile, const unsigned int aGpsRef, const bool aStrict){ 
////////////////////////////////////////////////////////////////////////////////////

  PrintAsciiLogo();
  gErrorIgnoreLevel = 3000;
  status_OK=true;

  // init timer
  time(&timer);
  timer_start=timer;

  // parse option file
  fOptionFile=aOptionFile;
  ReadOptions(aGpsRef, aStrict);

  // chunk FFT
  offt = new fft(tile->GetTimeRange()*triggers[0]->GetWorkingFrequency(), fftplan, "r2c");
 
  // data containers
  ChunkVect = new double [offt->GetSize_t()];
  TukeyWindow = GetTukeyWindow(offt->GetSize_t(),
                               (double)tile->GetOverlapDuration()/(double)tile->GetTimeRange());

  // default output directory: main dir
  maindir=fMaindir;
  for(unsigned int c=0; c<nchannels; c++){
    outdir.push_back(maindir);
  }
  
  // SG injection file
  if(fsginj){
    oinjfile.open((maindir+"/sginjections.txt").c_str());
    oinjfile<<"# Time # Frequency # Q # Amplitude # Phase # True SNR # sigma_t # sigma_f"<<endl;
    oinjfile.precision(5);
  }

  // default plottime offset
  SetPlotTimeOffset(0.0);

  // process monitoring
  chanindex      = -1;
  inSegments     = new Segments();
  outSegments    = new Segments* [nchannels];
  chunk_ctr      = 0;
  chan_ctr       = new unsigned int [nchannels];
  chan_data_ctr  = new unsigned int [nchannels];
  chan_cond_ctr  = new unsigned int [nchannels];
  chan_proj_ctr  = new unsigned int [nchannels];
  chan_write_ctr = new unsigned int [nchannels];
  chan_mapsnrmax = new double [nchannels];
  trig_ctr       = new unsigned int [nchannels];
  for(unsigned int c=0; c<nchannels; c++){
    outSegments[c]    = new Segments();
    chan_ctr[c]       = 0;
    chan_data_ctr[c]  = 0;
    chan_cond_ctr[c]  = 0;
    chan_proj_ctr[c]  = 0;
    chan_write_ctr[c] = 0;
    chan_mapsnrmax[c] = 0.0;
    trig_ctr[c]       = 0;
  }
}

////////////////////////////////////////////////////////////////////////////////////
Omicron::~Omicron(void){
////////////////////////////////////////////////////////////////////////////////////
  if(fVerbosity>1) cout<<"Omicron::~Omicron"<<endl;
  if(status_OK&&fOutProducts.find("summary")!=string::npos) SaveSummary(); // print final summary report
  if(status_OK&&fOutProducts.find("html")!=string::npos) MakeHtml(); // print final html report
  delete inSegments;
  delete chan_ctr;
  delete chan_data_ctr;
  delete chan_cond_ctr;
  delete chan_proj_ctr;
  delete chan_write_ctr;
  delete chan_mapsnrmax;
  delete trig_ctr;
  for(unsigned int c=0; c<nchannels; c++){
    delete outSegments[c];
    delete triggers[c];
    delete spectrum1[c];
    delete spectrum2[c];
  }
  delete outSegments;
  delete spectrum1;
  delete spectrum2;
  delete spectrumw;
  delete triggers;
  if(FFL_inject!=FFL&&FFL_inject!=NULL) delete FFL_inject;
  if(FFL!=NULL) delete FFL;
  if(inject!=NULL){
    for(int c=0; c<nchannels; c++) delete inject[c];
    delete inject;
  }
  delete oinj;
  delete tile;
  delete ChunkVect;
  delete TukeyWindow;
  delete offt;
  
  if(oinjfile.is_open()) oinjfile.close();
  outdir.clear();
  chunkcenter.clear();
  chunktfile.clear();
  fInjChan.clear();
  fInjFact.clear();
}

////////////////////////////////////////////////////////////////////////////////////
bool Omicron::InitSegments(Segments *aInSeg, Segments *aOutSeg){
////////////////////////////////////////////////////////////////////////////////////
  if(!status_OK){
    cerr<<"Omicron::InitSegments: the Omicron object is corrupted"<<endl;
    return false;
  }
  if(aInSeg==NULL){
    cerr<<"Omicron::InitSegments: the input segment is NULL"<<endl;
    return false;
  }
  if(!aInSeg->GetStatus()){
    cerr<<"Omicron::InitSegments: the input segment is corrupted"<<endl;
    return false;
  }
  if(!aInSeg->GetN()){
    cerr<<"Omicron::InitSegments: there is no input segment"<<endl;
    return false;
  }

  if(fVerbosity) cout<<"Omicron::InitSegments: initiate data segments..."<<endl;

  // cumulative input segment monitor
  if(!inSegments->AddSegments(aInSeg)) return false;
  
  // set sequence structure
  if(!tile->SetSegments(aInSeg, aOutSeg)) return false;

  // update channel list
  if(FFL!=NULL){
    if(!FFL->ExtractChannels(aInSeg->GetStart(0))) return false;
  }
  if(FFL_inject!=NULL&&FFL_inject!=FFL){
    if(!FFL_inject->ExtractChannels(aInSeg->GetStart(0))) return false;
  }

  if(fVerbosity>1){
    cout<<"\t- N input segments   = "<<aInSeg->GetN()<<endl;
    cout<<"\t- Input livetime     = "<<aInSeg->GetLiveTime()<<endl;
    if(aOutSeg!=NULL){
      cout<<"\t- N output segments  = "<<aOutSeg->GetN()<<endl;
      cout<<"\t- Output livetime    = "<<aOutSeg->GetLiveTime()<<endl;
    }
  }
  
  return true;
}

////////////////////////////////////////////////////////////////////////////////////
bool Omicron::MakeDirectories(const double aId){
////////////////////////////////////////////////////////////////////////////////////
  if(!status_OK){
    cerr<<"Omicron::MakeDirectories: the Omicron object is corrupted"<<endl;
    return false;
  }

  if(fVerbosity) cout<<"Omicron::MakeDirectories: make directory structure..."<<endl;
  stringstream tmpstream;

  // Main dir
  if(aId==0.0){// single level
    maindir=fMaindir;
  }
  else{// two level
    tmpstream<<fMaindir<<"/"<<setprecision(3)<<fixed<<aId;
    maindir=tmpstream.str();
    tmpstream.clear(); tmpstream.str("");
    if(system(("mkdir -p "+maindir).c_str())!=0){
      cerr<<"Omicron::MakeDirectories: the output directory cannot be created"<<endl;
      cerr<<"                          - this error can be the result of missing RAM -"<<endl;
      return false;
    }
  }

  // channel directories
  outdir.clear();
  for(unsigned int c=0; c<nchannels; c++){
    outdir.push_back(maindir+"/"+triggers[c]->GetName());
    if(system(("mkdir -p "+outdir[c]).c_str())!=0){
      cerr<<"Omicron::MakeDirectories: the output directory cannot be created"<<endl;
      cerr<<"                          - this error can be the result of missing RAM -"<<endl;
      return false;
    }
  }

  // update SG injections file
  if(oinjfile.is_open()){
    oinjfile.close();
    oinjfile.open((maindir+"/sginjections.txt").c_str());
    oinjfile<<"# Time # Frequency # Q # Amplitude # Phase # True SNR # sigma_t # sigma_f"<<endl;
    oinjfile.precision(5);
  }
    
  return true;
}

////////////////////////////////////////////////////////////////////////////////////
bool Omicron::NewChunk(void){
////////////////////////////////////////////////////////////////////////////////////
  if(!status_OK){
    cerr<<"Omicron::NewChunk: the Omicron object is corrupted"<<endl;
    return false;
  }

  bool newseg;
  
  // load new chunk
  if(fVerbosity) cout<<"Omicron::NewChunk: call a new chunk..."<<endl;
  if(!tile->NewChunk(newseg)) return false;
  if(fVerbosity>1){
    if(newseg) cout<<"\t- chunk "<<tile->GetChunkTimeStart()<<"-"<<tile->GetChunkTimeEnd()<<" is loaded (start a new segment)"<<endl;
    else cout<<"\t- chunk "<<tile->GetChunkTimeStart()<<"-"<<tile->GetChunkTimeEnd()<<" is loaded"<<endl;
  }

  // save info for html report
  if(fOutProducts.find("html")!=string::npos) chunkcenter.push_back(tile->GetChunkTimeCenter());

  // new segment --> reset PSD buffer
  if(newseg){
    for(unsigned int c=0; c<nchannels; c++){
      spectrum1[c]->Reset();
      spectrum2[c]->Reset();
    }
  }

  // generate SG parameters
  if(fsginj) oinj->MakeWaveform();

  // one more chunk
  chunk_ctr++;
  
  return true;
}

////////////////////////////////////////////////////////////////////////////////////
bool Omicron::DefineNewChunk(const unsigned int aTimeStart, const unsigned int aTimeEnd, const bool aResetPsdBuffer){
////////////////////////////////////////////////////////////////////////////////////
  if(!status_OK){
    cerr<<"Omicron::DefineNewChunk: the Omicron object is corrupted"<<endl;
    return false;
  }
  if(aTimeEnd-aTimeStart!=tile->GetTimeRange()){
    cerr<<"Omicron::DefineNewChunk: the input chunk is not the right duration"<<endl;
    return false;
  }

  if(fVerbosity) cout<<"Omicron::DefineNewChunk: "<<aTimeStart<<"-"<<aTimeEnd<<endl;

  // set segment for this chunk
  Segments *Stmp = new Segments((double)aTimeStart, (double)aTimeEnd);
  if(!InitSegments(Stmp)){
    delete Stmp;
    return false;
  }
  delete Stmp;

  // set tiling for this segment
  bool dummy; // not used
  if(!tile->NewChunk(dummy)) return false;
    
  // save info for html report
  if(fOutProducts.find("html")!=string::npos) chunkcenter.push_back(tile->GetChunkTimeCenter());

  // reset PSD buffer
  if(aResetPsdBuffer){
    for(unsigned int c=0; c<nchannels; c++){
      spectrum1[c]->Reset();
      spectrum2[c]->Reset();
    }
  }
      
  // generate SG parameters
  if(fsginj) oinj->MakeWaveform();

  // one more chunk
  chunk_ctr++;
  
  return true;
}

////////////////////////////////////////////////////////////////////////////////////
bool Omicron::NewChannel(void){
////////////////////////////////////////////////////////////////////////////////////
  if(!status_OK){
    cerr<<"Omicron::NewChannel: the Omicron object is corrupted"<<endl;
    return false;
  }

  // load new channel
  if(fVerbosity) cout<<"Omicron::NewChannel: load a new channel..."<<endl;
  chanindex++;

  // last channel
  if(chanindex==(int)nchannels){
    chanindex=-1;
    if(fVerbosity>1) cout<<"\t- no more channels to load"<<endl;
    return false; 
  }

  // new channel
  if(fVerbosity>1) cout<<"\t- channel "<<triggers[chanindex]->GetName()<<" is loaded"<<endl;
  chan_ctr[chanindex]++;

  // reset number of tiles above threshold
  trig_ctr[chanindex]=0;
  
  return true;
}

////////////////////////////////////////////////////////////////////////////////////
bool Omicron::LoadData(double **aDataVector, unsigned int &aSize){
////////////////////////////////////////////////////////////////////////////////////
  if(!status_OK){
    cerr<<"Omicron::LoadData: the Omicron object is corrupted"<<endl;
    *aDataVector=NULL; aSize=0;
    return false;
  }
  if(FFL==NULL){
    cerr<<"Omicron::LoadData: this function can only be used with a valid FFL object"<<endl;
    *aDataVector=NULL; aSize=0;
    return false;
  }
  if(!tile->GetChunkTimeCenter()){
    cerr<<"Omicron::LoadData: no chunk called yet"<<endl;
    *aDataVector=NULL; aSize=0;
    return false;
  }
  if(chanindex<0){
    cerr<<"Omicron::LoadData: no channel called yet"<<endl;
    *aDataVector=NULL; aSize=0;
    return false;
  }

  if(fVerbosity){
    if(fInjChan.size()) cout<<"Omicron::LoadData: load data vector and add injections..."<<endl;
    else cout<<"Omicron::LoadData: load data vector..."<<endl;
  }

  // get data vector
  if(fVerbosity>1) cout<<"\t- get data from frames..."<<endl;
  *aDataVector = FFL->GetData(aSize, triggers[chanindex]->GetName(),
                             tile->GetChunkTimeStart(), tile->GetChunkTimeEnd());
  
  // cannot retrieve data
  if(aSize<=0){
    *aDataVector=NULL; aSize=0;
    return false;
  }

  // test native sampling (and update if necessary)
  unsigned int nativesampling = aSize/(tile->GetTimeRange());
  if(!triggers[chanindex]->SetNativeFrequency(nativesampling)) return false;

  // add software injections if any
  if(inject!=NULL){
    if(fVerbosity>1) cout<<"\t- perform software injections..."<<endl;
    inject[chanindex]->Inject(aSize, *aDataVector, tile->GetChunkTimeStart());
  }
  
  // get injection vector and inject it
  if(fInjChan.size()){
    if(fVerbosity>1) cout<<"\t- perform stream injections..."<<endl;
    unsigned int dsize_inj;
    double *dvector_inj = FFL_inject->GetData(dsize_inj, fInjChan[chanindex],
                                              tile->GetChunkTimeStart(), tile->GetChunkTimeEnd());

    // cannot retrieve data
    if(dsize_inj<=0){
      delete *aDataVector;
      *aDataVector=NULL; aSize=0;
      return false;
    }

    // size mismatch
    if(dsize_inj!=aSize){
      cerr<<"Omicron::LoadData: the sampling of the injection channel is not the same as the sampling of the main channel ("<<fInjChan[chanindex]<<" "<<tile->GetChunkTimeStart()<<"-"<<tile->GetChunkTimeEnd()<<")"<<endl;
      delete *aDataVector; *aDataVector=NULL; aSize=0;
      delete dvector_inj;
      return false;
    }

    // add injections in the data
    for(unsigned int d=0; d<aSize; d++) (*aDataVector)[d] += (fInjFact[chanindex]*dvector_inj[d]);
    delete dvector_inj;
  }

  chan_data_ctr[chanindex]++;
  return true;
}

////////////////////////////////////////////////////////////////////////////////////
int Omicron::Condition(const unsigned int aInVectSize, double *aInVect, bool &aIsFlat){
////////////////////////////////////////////////////////////////////////////////////
  if(!status_OK){
    cerr<<"Omicron::Condition: the Omicron object is corrupted"<<endl;
    return -1;
  }

  // Input data checks
  if(aInVect==NULL){
    cerr<<"Omicron::Condition: input vector is NULL ("<<triggers[chanindex]->GetName()<<" "<<tile->GetChunkTimeStart()<<"-"<<tile->GetChunkTimeEnd()<<")"<<endl;
    return 1;
  }

  // input vector is flat
  aIsFlat=false;
  if(aInVectSize>0&&IsFlat(aInVectSize, aInVect)){
    aIsFlat=true;
    cerr<<"Omicron::Condition: input vector is flat ("<<triggers[chanindex]->GetName()<<" "<<tile->GetChunkTimeStart()<<"-"<<tile->GetChunkTimeEnd()<<")"<<endl;
  }
  
  if(fVerbosity) cout<<"Omicron::Condition: condition data vector..."<<endl;

  // test native sampling (and update if necessary)
  unsigned int nativesampling = aInVectSize/(tile->GetTimeRange());
  if(!triggers[chanindex]->SetNativeFrequency(nativesampling)) return 2;

  // add sg injections
  if(fsginj){
    if(fVerbosity>1) cout<<"\t- add SineGaus injections..."<<endl;
    for(unsigned int d=0; d<aInVectSize; d++)
      aInVect[d] += oinj->GetWaveform(d, nativesampling);
  }

  // transform data vector
  if(fVerbosity>1) cout<<"\t- transform data vector..."<<endl;
  if(!triggers[chanindex]->Transform(aInVectSize, aInVect,
                                     offt->GetSize_t(), ChunkVect)) return 3;

  // apply Tukey Window
  if(fVerbosity>1) cout<<"\t- apply Tukey window..."<<endl;
  for(unsigned int i=0; i<offt->GetSize_t(); i++)
    ChunkVect[i] *= TukeyWindow[i];

  // update first spectrum (if enough data)
  if(fVerbosity>1) cout<<"\t- update spectrum 1..."<<endl;
  unsigned int dstart = (tile->GetCurrentOverlapDuration()-tile->GetOverlapDuration()/2)*triggers[chanindex]->GetWorkingFrequency(); // start of 'sane' data
  unsigned int dsize = (tile->GetTimeRange()-tile->GetCurrentOverlapDuration())*triggers[chanindex]->GetWorkingFrequency(); // size of 'sane' data
  if(!spectrum1[chanindex]->AddData(dsize, ChunkVect, dstart))
     cerr<<"Omicron::Condition: warning: this chunk is not used for PSD(1) estimation ("<<triggers[chanindex]->GetName()<<" "<<tile->GetChunkTimeStart()<<"-"<<tile->GetChunkTimeEnd()<<")"<<endl;

  // fft-forward the chunk data
  if(fVerbosity>1) cout<<"\t- move the data in the frequency domain..."<<endl;
  if(!offt->Forward(ChunkVect)) return 4;

  // 1st whitening (+ FFT normalization)
  if(fVerbosity>1) cout<<"\t- whiten chunk (1)"<<endl;
  Whiten(spectrum1[chanindex], 1.0/(double)offt->GetSize_t());

  // back in the time domain
  if(fVerbosity>1) cout<<"\t- move the data back in the time domain..."<<endl;
  offt->Backward();

  // update second spectrum
  if(fVerbosity>1) cout<<"\t- update spectrum 2..."<<endl;
  double *rvec = offt->GetRe_t();
  if(!spectrum2[chanindex]->AddData(dsize, rvec, dstart))
    cerr<<"Omicron::Condition: warning: this chunk is not used for PSD(2) estimation ("<<triggers[chanindex]->GetName()<<" "<<tile->GetChunkTimeStart()<<"-"<<tile->GetChunkTimeEnd()<<")"<<endl;
  delete rvec;
  
  // fft-forward the chunk data
  if(fVerbosity>1) cout<<"\t- move the data in the frequency domain..."<<endl;
  offt->Forward();
  // This is mandatory because the frequency-domain vector was messed-up by the previous backward (r2c)

  // 2nd whitening (+ FFT normalization)
  if(fVerbosity>1) cout<<"\t- whiten chunk (2)"<<endl;
  Whiten(spectrum2[chanindex], 1.0/(double)triggers[chanindex]->GetWorkingFrequency());

  // compute tiling power
  if(fVerbosity>1) cout<<"\t- compute tiling power..."<<endl;
  tile->SetPower(spectrum1[chanindex], spectrum2[chanindex]);

  // save sg injection parameters
  // must be done here because the spectra are needed
  if(fsginj) SaveSG();

  chan_cond_ctr[chanindex]++;
  return 0;
}

////////////////////////////////////////////////////////////////////////////////////
long unsigned int Omicron::Project(void){
////////////////////////////////////////////////////////////////////////////////////
  trig_ctr[chanindex]=0;
  if(!status_OK){
    cerr<<"Omicron::Project: the Omicron object is corrupted"<<endl;
    return 0;
  }

  if(fVerbosity) cout<<"Omicron::Project: project data onto the tiles..."<<endl;
  chan_proj_ctr[chanindex]++;
  trig_ctr[chanindex] = tile->ProjectData(offt);
  return trig_ctr[chanindex];
}

////////////////////////////////////////////////////////////////////////////////////
bool Omicron::WriteOutput(void){
////////////////////////////////////////////////////////////////////////////////////
  if(!status_OK){
    cerr<<"Omicron::WriteOutput: the Omicron object is corrupted"<<endl;
    return false;
  }
  if(fVerbosity) cout<<"Omicron::WriteOutput: write chunk output..."<<endl;
 
  //*** ASD
  if(fOutProducts.find("asd")!=string::npos){
    if(fVerbosity>1) cout<<"\t- write ASD..."<<endl;
    SaveAPSD("ASD");
  }
  
  //*** PSD
  if(fOutProducts.find("psd")!=string::npos){
    if(fVerbosity>1) cout<<"\t- write PSD..."<<endl;
    SaveAPSD("PSD");
  }
    
  //*** CONDITIONNED TS
  if(fOutProducts.find("timeseries")!=string::npos){
    if(fVerbosity>1) cout<<"\t- write conditionned time-series..."<<endl;
    SaveTS(false);
  }

  //*** WHITENED TS
  if(fOutProducts.find("white")!=string::npos){
    if(fVerbosity>1) cout<<"\t- write whitened data..."<<endl;
    offt->Backward();// Back in time domain
    // IMPORTANT: after that, the frequency-domain vector of offt is corrupted (r2c)
    // apply FFT normalization
    for(unsigned int i=0; i<offt->GetSize_t(); i++)
      offt->SetRe_t(i, offt->GetRe_t(i)*(double)triggers[chanindex]->GetWorkingFrequency()/(double)offt->GetSize_t());
    SaveTS(true);

    if(fOutProducts.find("whitepsd")!=string::npos){
      unsigned int dstart = (tile->GetCurrentOverlapDuration()-tile->GetOverlapDuration()/2)*triggers[chanindex]->GetWorkingFrequency(); // start of 'sane' data
      unsigned int dsize = (tile->GetTimeRange()-tile->GetCurrentOverlapDuration())*triggers[chanindex]->GetWorkingFrequency(); // size of 'sane' data
      if(spectrumw->LoadData(dsize, offt->GetRe_t(), dstart)) SaveWPSD();
    }
  }
  
  //*** MAPS
  if(fOutProducts.find("map")!=string::npos){
    if(fVerbosity>1) cout<<"\t- write maps"<<endl;
    double snr=tile->SaveMaps(outdir[chanindex],
                              triggers[chanindex]->GetNameConv()+"_OMICRON",
                              fOutFormat,toffset,(bool)(fOutProducts.find("html")+1));
    if(snr>chan_mapsnrmax[chanindex]) chan_mapsnrmax[chanindex]=snr;// get snr max over all chunks
  }

  //*** TRIGGERS
  if(fOutProducts.find("triggers")!=string::npos){
    if(fVerbosity>1) cout<<"\t- write triggers "<<endl;
    double triggerrate = 0.0;
    if((!ExtractTriggers(triggerrate)) || (FlushTriggers()<0)){// extract triggers
      chunktfile.push_back("");
      return false;
    }
    // write triggers to disk
    chunktfile.push_back(GetFileNameFromPath(WriteTriggers()));
  }

  // update monitoring segments
  // -- do not include output segment selection --
  outSegments[chanindex]->AddSegment((double)(tile->GetChunkTimeStart()+tile->GetCurrentOverlapDuration()-tile->GetOverlapDuration()/2),(double)(tile->GetChunkTimeEnd()-tile->GetOverlapDuration()/2));

  chan_write_ctr[chanindex]++;
  return true;
}

////////////////////////////////////////////////////////////////////////////////////
bool Omicron::ExtractTriggers(double &aTriggerRate){
////////////////////////////////////////////////////////////////////////////////////
  if(!status_OK){
    cerr<<"Omicron::ExtractTriggers: the Omicron object is corrupted"<<endl;
    return false;
  }

  // trigger rate: evaluated over the chunk excluding nominal overlaps/2
  aTriggerRate = (double)trig_ctr[chanindex]/(double)(tile->GetTimeRange()-tile->GetOverlapDuration());

  if(fVerbosity>1) cout<<"Omicron::ExtractTriggers: "<<trig_ctr[chanindex]<<" triggers"<<endl;

  // check against trigger rate limit
  if(aTriggerRate>fratemax){
    cerr<<"Omicron::ExtractTriggers: the maximum trigger rate ("<<fratemax<<" Hz) is exceeded ("<<triggers[chanindex]->GetName()<<" "<<tile->GetChunkTimeStart()<<"-"<<tile->GetChunkTimeEnd()<<")"<<endl;
    return false;
  }

  // save tiles above SNR threshold
  if(!tile->SaveTriggers(triggers[chanindex])){
    triggers[chanindex]->TriggerBuffer::Reset();
    triggers[chanindex]->Triggers::Reset();
    return false;
  }
  
  return true;
}

////////////////////////////////////////////////////////////////////////////////////
Long64_t Omicron::FlushTriggers(void){
////////////////////////////////////////////////////////////////////////////////////
  if(!status_OK){
    cerr<<"Omicron::FlushTriggers: the Omicron object is corrupted"<<endl;
    return -1;
  }

  if(fVerbosity>1) cout<<"Omicron::FlushTriggers"<<endl;
  
  // flush buffer if any
  if(!triggers[chanindex]->Flush()) return -1;
  if(fVerbosity>2) cout<<"\t - flushed"<<endl;

  // sort triggers
  if(!triggers[chanindex]->SortTriggers()) return -1;
  if(fVerbosity>2) cout<<"\t - triggers sorted"<<endl;

  // clustering if any
  if(fClusterAlgo.compare("none")){
    if(!triggers[chanindex]->Clusterize()) return -1;
    if(fVerbosity>2) cout<<"\t - "<<triggers[chanindex]->GetClusterN()<<" clusters"<<endl;
    return (Long64_t)triggers[chanindex]->GetClusterN();
  }
  if(fVerbosity>2) cout<<"\t - "<<triggers[chanindex]->Triggers::GetTriggerN()<<" triggers"<<endl;

  return triggers[chanindex]->Triggers::GetTriggerN();
}

////////////////////////////////////////////////////////////////////////////////////
string Omicron::WriteTriggers(const bool aUseLVDir){
////////////////////////////////////////////////////////////////////////////////////
  if(!status_OK){
    cerr<<"Omicron::WriteTriggers: the Omicron object is corrupted"<<endl;
    return "";
  }

  if(fVerbosity>1) cout<<"Omicron::WriteTriggers"<<endl;

  // write triggers to disk
  if(!aUseLVDir)
    return triggers[chanindex]->Write(outdir[chanindex], fOutFormat);

  unsigned int gps_5=0;
  if(triggers[chanindex]->GetN()) gps_5 = (unsigned int) (triggers[chanindex]->GetFirst() / 100000);
  else return "";
  
  stringstream lv_dir;
  lv_dir << maindir << "/" << triggers[chanindex]->GetNamePrefix() << "/" << triggers[chanindex]->GetNameSuffixUnderScore() << "_OMICRON/" << gps_5;
  if(system(("mkdir -p " + lv_dir.str()).c_str())!=0) return "";
  return triggers[chanindex]->Write(lv_dir.str(), fOutFormat);  
}

////////////////////////////////////////////////////////////////////////////////////
void Omicron::Whiten(Spectrum *aSpec, const double aNorm){
////////////////////////////////////////////////////////////////////////////////////

  unsigned int i=0; // frequency index
 
  // zero-out DC
  offt->SetRe_f(i,0.0);
  offt->SetIm_f(i,0.0);
  i++;
  
  // zero-out below highpass frequency
  unsigned int n = (unsigned int)(triggers[chanindex]->GetHighPassFrequency()*(double)tile->GetTimeRange());
  for(; i<n; i++){
    offt->SetRe_f(i,0.0);
    offt->SetIm_f(i,0.0);
  }
 
  // normalize data by the ASD
  double asdval;
  for(; i<offt->GetSize_f(); i++){
    asdval=aSpec->GetAmplitude((double)i/(double)tile->GetTimeRange())/sqrt(2.0);
    if(asdval<=0){
      offt->SetRe_f(i,0.0);
      offt->SetIm_f(i,0.0);
      continue;
    }
    offt->SetRe_f(i, offt->GetRe_f(i) * aNorm / asdval);
    offt->SetIm_f(i, offt->GetIm_f(i) * aNorm / asdval);
  }

  return;
}




