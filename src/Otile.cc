/**
 * @file 
 * @brief See Otile.h
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#include "Otile.h"

ClassImp(Otile)

////////////////////////////////////////////////////////////////////////////////////
Otile::Otile(const unsigned int aTimeRange,
	     const double aQMin, const double aQMax, 
	     const double aFrequencyMin, const double aFrequencyMax, 
	     const unsigned int aSampleFrequency, const double aMaximumMismatch, 
	     const string aPlotStyle, const unsigned int aVerbosity):
GwollumPlot("otile",aPlotStyle){
////////////////////////////////////////////////////////////////////////////////////
 
  // save parameters
  fVerbosity = aVerbosity;
  MaximumMismatch = aMaximumMismatch;
  
  // compute Q values
  vector <double> Qs = ComputeQs(aQMin, aQMax, MaximumMismatch);
  nq = Qs.size();
 
  // create Q planes
  if(fVerbosity) cout<<"Otile::Otile: creating "<<nq<<" Q-planes"<<endl;
  qplanes = new Oqplane* [nq];
  t_snrmax = new unsigned int* [nq];
  f_snrmax = new unsigned int* [nq];
  for(unsigned int q=0; q<nq; q++){
    qplanes[q] = new Oqplane(Qs[q], aSampleFrequency,
                             aFrequencyMin, aFrequencyMax,
                             aTimeRange, MaximumMismatch);
    if(aVerbosity>1) qplanes[q]->PrintParameters();
  }
  Qs.clear();

  // set default parameters
  SetSnrThr();
  SetMapFill();
  SetRangez();

  // plot time window
  pwin.push_back(GetTimeRange());

  // full map parameters
  FullMapNt = 301;
  FullMapFrequencyMin = GetFrequencyMin();
  FullMapFrequencyMax = GetFrequencyMax();
  FullMapFrequencyLogStep = log(FullMapFrequencyMax/FullMapFrequencyMin) / (double)GetBandN(nq);

  // Newtonian chirp track
  chirp = new TF1("chirp", "pow([0]*(x-[1]), -3.0/8.0)", -(double)GetTimeRange()/2.0, -0.0001);
  chirp->SetLineColor(0);
  chirp->SetLineWidth(1);
  chirp->SetNpx(1000);
  SetChirp();

  // init sequence
  SeqInSegments  = new Segments();
  SeqOutSegments = new Segments();
  SeqOverlap=0;
  SeqOverlapCurrent=SeqOverlap;
  SeqT0=0;
  SeqSeg=0;
}

////////////////////////////////////////////////////////////////////////////////////
Otile::~Otile(void){
////////////////////////////////////////////////////////////////////////////////////
  if(fVerbosity>1) cout<<"Otile::~Otile"<<endl;
  for(unsigned int q=0; q<nq; q++){
    delete qplanes[q];
  }
  delete qplanes;
  delete t_snrmax;
  delete f_snrmax;
  delete SeqInSegments;
  delete SeqOutSegments;
  delete chirp;
}
 
////////////////////////////////////////////////////////////////////////////////////
long unsigned int Otile::GetTileN(const double aPadding){
////////////////////////////////////////////////////////////////////////////////////
  long unsigned int nt = 0;
  for(unsigned int q=0; q<nq; q++){
    nt+=qplanes[q]->GetTileN(aPadding);
  }
  return nt;  
}

////////////////////////////////////////////////////////////////////////////////////
bool Otile::SetSegments(Segments *aInSeg, Segments *aOutSeg){
////////////////////////////////////////////////////////////////////////////////////
  if(aInSeg==NULL || !aInSeg->GetStatus()){
    cerr<<"Otile::SetSegments: input segments are corrupted"<<endl;
    return false;
  }

  // reset sequence
  SeqInSegments->Reset(); SeqOutSegments->Reset();
  SeqOverlapCurrent=SeqOverlap;
  SeqT0=0;// for initialization in NewChunk()
  SeqSeg=0;

  // update sequence
  SeqInSegments->Append(aInSeg);
  if(aOutSeg==NULL) SeqOutSegments->Append(SeqInSegments);// no selection
  else SeqOutSegments->Append(aOutSeg);

  return true;
}

////////////////////////////////////////////////////////////////////////////////////
bool Otile::NewChunk(bool &aNewSegFlag){
////////////////////////////////////////////////////////////////////////////////////

  // no more segments
  if(SeqSeg>=SeqInSegments->GetN()){
    cerr<<"Otile::NewChunk: end of segments"<<endl;
    return false;
  }
  
  // current segment is too short
  if((unsigned int)SeqInSegments->GetLiveTime(SeqSeg)<GetTimeRange()){
    SeqSeg++; //  --> move to next segment
    SeqT0=0;  // flag a new segment start
    return NewChunk(aNewSegFlag);
  }

  // end of current segment
  if(SeqT0+GetTimeRange()/2==(unsigned int)SeqInSegments->GetEnd(SeqSeg)){
    SeqSeg++; //  --> move to next segment
    SeqT0=0;  // flag a new segment start
    return NewChunk(aNewSegFlag);
  }

  // initialization = start of current segment
  if(SeqT0==0){
    SeqT0=(unsigned int)SeqInSegments->GetStart(SeqSeg)-GetTimeRange()/2+SeqOverlap;
    aNewSegFlag=true;
  }
  else aNewSegFlag=false;

  // new test chunk
  SeqOverlapCurrent = SeqOverlap;// reset current overlap
  unsigned int start_test = SeqT0+GetTimeRange()/2-SeqOverlap;
  unsigned int stop_test  = start_test+GetTimeRange();

  // chunk ends after current segment end --> adjust overlap
  if(stop_test>(unsigned int)SeqInSegments->GetEnd(SeqSeg)){
    SeqT0=(unsigned int)SeqInSegments->GetEnd(SeqSeg)-GetTimeRange()/2;
    SeqOverlapCurrent=start_test+SeqOverlap-SeqT0+GetTimeRange()/2;// --> adjust overlap
    return true;
  }

  // OK  
  SeqT0=start_test+GetTimeRange()/2;
  return true;
}

////////////////////////////////////////////////////////////////////////////////////
void Otile::DrawMapTiling(const unsigned int aQindex){
////////////////////////////////////////////////////////////////////////////////////  
  qplanes[aQindex]->FillMap("display",qplanes[aQindex]->GetTimeMin(),qplanes[aQindex]->GetTimeMax());
  GwollumPlot::Draw(qplanes[aQindex]->tfmap,"COL");
  /*
  qplanes[aQindex]->GetXaxis()->SetRangeUser(-0.1,0.1);
  TLine *lf;
  for(int f=0; f<qplanes[aQindex]->GetNBands(); f++){
    lf = new TLine(-0.1,qplanes[aQindex]->GetBandStart(f),0.1,qplanes[aQindex]->GetBandStart(f));
    lf->SetLineStyle(3);
    lf->SetLineColor(2);
    Draw(lf,"same");
  }
  */
  return;
}

////////////////////////////////////////////////////////////////////////////////////
long unsigned int Otile::ProjectData(fft *aDataFft){
////////////////////////////////////////////////////////////////////////////////////

  long unsigned int nt=0;// number of tiles above threshold
  
  // project onto q planes, removing half the overlap on both sides
  for(unsigned int p=0; p<nq; p++)
    nt+=qplanes[p]->ProjectData(aDataFft, (double)(SeqOverlap/2));
  
  return nt;
}

////////////////////////////////////////////////////////////////////////////////////
Segments* Otile::GetTileSegments(TH1D *aSnrThreshold, const double aPadding){
////////////////////////////////////////////////////////////////////////////////////
  Segments *seg= new Segments();
  
  // add tile segments for each plane
  for(unsigned int p=0; p<nq; p++)
    qplanes[p]->AddTileSegments(seg, aSnrThreshold, (double)SeqT0, aPadding);
    
  return seg;
}

////////////////////////////////////////////////////////////////////////////////////
bool Otile::SaveTriggers(TriggerBuffer *aTriggers){
////////////////////////////////////////////////////////////////////////////////////
  if(!aTriggers->Segments::GetStatus()){
    cerr<<"Otile::SaveTriggers: the trigger Segments object is corrupted"<<endl;
    return false;
  }
  
  // output segments (remove 1/2 overlap)
  Segments *seg = new Segments((double)(SeqT0-GetTimeRange()/2+SeqOverlapCurrent-SeqOverlap/2),
                               (double)(SeqT0+GetTimeRange()/2-SeqOverlap/2));

  // apply user-defined output selection
  if(!seg->Intersect(SeqOutSegments)){
    delete seg;
    return false;
  }
  
  // no livetime --> nothing to do
  if(!seg->GetLiveTime()){
    delete seg;
    return true;
  }

  if(fVerbosity) cout<<"Otile::SaveTriggers: Saving triggers for "<<aTriggers->GetName()<<" in "<<seg->GetFirst()<<"-"<<seg->GetLast()<<endl;

  // if buffer, segments must be saved first
  if(aTriggers->GetSize()){
    if(!aTriggers->SetSegments(seg)){ delete seg; return false; }
  }
  
  // save triggers for each Q plane
  for(unsigned int p=0; p<nq; p++){
    if(!qplanes[p]->SaveTriggers(aTriggers, (double)SeqT0, seg)){ delete seg; return false; }
  }
    
  // save trigger segments (if no buffer)
  if(aTriggers->GetSize()==0){
    aTriggers->Segments::AddSegments(seg);
  }
  
  delete seg;
  return true;
}

////////////////////////////////////////////////////////////////////////////////////
double Otile::SaveMaps(const string aOutdir, const string aName, const string aFormat,
                       const double aTimeOffset, const bool aThumb){
////////////////////////////////////////////////////////////////////////////////////
  if(!IsDirectory(aOutdir)){
    cerr<<"Otile::SaveMaps: the directory "<<aOutdir<<" is missing"<<endl;
    return -1.0;
  }
   
  if(fVerbosity) cout<<"Otile::SaveMaps: Saving maps for "<<aName<<" centered on "<<(double)SeqT0+aTimeOffset<<"..."<<endl;

  // get SNR-max bins /qplane and /window
  unsigned int tstart, tend;
  double snr2max, snr2;
  for(unsigned int q=0; q<nq; q++){

    // max value for each window
    t_snrmax[q] = new unsigned int [pwin.size()];
    f_snrmax[q] = new unsigned int [pwin.size()];

    // loop over windows
    for(unsigned int w=0; w<pwin.size(); w++){

      // init
      snr2max =-1.0;
      t_snrmax[q][w] = 0;
      f_snrmax[q][w] = 0;

      // loop over frequency rows
      for(unsigned int f=0; f<qplanes[q]->GetBandN(); f++){

        // window starting/ending tile index
	tstart=qplanes[q]->GetTimeTileIndex(f,aTimeOffset-(double)pwin[w]/2.0);
	tend=qplanes[q]->GetTimeTileIndex(f,aTimeOffset+(double)pwin[w]/2.0);

	for(unsigned int t=tstart; t<tend; t++){
	  snr2=qplanes[q]->GetTileSnrSq(t,f);
	  if(snr2>snr2max){
	    snr2max=snr2;
	    t_snrmax[q][w] = t;
	    f_snrmax[q][w] = f;
	  }
	}
      }
    }
  }

  // get qplane with SNR-max /window (for full map)
  unsigned int *q_snrmax = new unsigned int [pwin.size()];
  for(unsigned int w=0; w<pwin.size(); w++){
    snr2max=-1.0;
    q_snrmax[w]=0;
    for(unsigned int q=0; q<nq; q++){
      snr2=qplanes[q]->GetTileSnrSq(t_snrmax[q][w],f_snrmax[q][w]);
      if(snr2>snr2max){
	snr2max=snr2;
	q_snrmax[w]=q;
      }
    }
  }

  // maximum SNR in the first window
  double snr_max_w0 = TMath::Sqrt(qplanes[q_snrmax[0]]->GetTileSnrSq(t_snrmax[q_snrmax[0]][0],f_snrmax[q_snrmax[0]][0]));

  // apply map SNR threshold (first window only!)
  if(qplanes[q_snrmax[0]]->GetTileSnrSq(t_snrmax[q_snrmax[0]][0],f_snrmax[q_snrmax[0]][0])<SnrThr_map*SnrThr_map){
    // below threshold: exit
    if(fVerbosity) cout<<"Otile::SaveMaps: map "<<aName<<" (+-"<<pwin[0]/2.0<<"s) is below SNR threshold -> do not save"<<endl;
    delete q_snrmax;
    for(unsigned int q=0; q<nq; q++){
      delete t_snrmax[q];
      delete f_snrmax[q];
    }
    return snr_max_w0;
  }

  // graphix
  vector <string> form;
  if(aFormat.find("gif")!=string::npos) form.push_back("gif");
  if(aFormat.find("png")!=string::npos) form.push_back("png");
  if(aFormat.find("pdf")!=string::npos) form.push_back("pdf");
  if(aFormat.find("ps")!=string::npos)  form.push_back("ps");
  if(aFormat.find("xml")!=string::npos) form.push_back("xml");
  if(aFormat.find("eps")!=string::npos) form.push_back("eps"); 
  if(aFormat.find("jpg")!=string::npos) form.push_back("jpg"); 
  if(aFormat.find("svg")!=string::npos) form.push_back("svg");
  if(aFormat.find("root")!=string::npos) form.push_back("root");
  
  // no graphical format --> exit
  if(form.size()==0){
    delete q_snrmax;
    for(unsigned int q=0; q<nq; q++){
      delete t_snrmax[q];
      delete f_snrmax[q];
    }
    return snr_max_w0;
  }
  
  // fill maps
  for(unsigned int q=0; q<nq; q++)
    qplanes[q]->FillMap(mapfill,
                        aTimeOffset-(double)pwin.back()/2.0,
                        aTimeOffset+(double)pwin.back()/2.0);

  // set chirp parameters
  if(chirpt<=0) chirp->SetParameter(1, (double)SeqT0+aTimeOffset);
  else chirp->SetParameter(1, chirpt);

  // cosmetics
  GwollumPlot::SetLogx(0);
  GwollumPlot::SetLogy(1);
  GwollumPlot::SetGridx(0);
  GwollumPlot::SetGridy(0);

  // special case of root files
  stringstream tmpstream;
  TFile *frootmap;
  if(aFormat.find("root")!=string::npos){
    tmpstream<<aOutdir<<"/"<<aName<<"MAPQ-"<<SeqT0<<".root";
    frootmap = new TFile(tmpstream.str().c_str(),"RECREATE");
    tmpstream.clear(); tmpstream.str("");
  }
  
  // save maps for each Q plane
  for(unsigned int q=0; q<nq; q++){
    if(fVerbosity>1) cout<<"\t- map Q"<<q<<" = "<<fixed<<setprecision(3)<<qplanes[q]->GetQ()<<endl;
    if(fVerbosity>2) cout<<"\t\t- Draw map"<<endl;

    // apply time offset
    qplanes[q]->ApplyOffset((double)SeqT0);

    // last window zoom
    qplanes[q]->tfmap->GetXaxis()->SetRange((double)SeqT0+aTimeOffset-(double)pwin.back()/2.0,
                                     (double)SeqT0+aTimeOffset+(double)pwin.back()/2.0);
    // plot title
    tmpstream<<aName<<": Q="<<fixed<<setprecision(3)<<qplanes[q]->GetQ();
    qplanes[q]->tfmap->SetTitle(tmpstream.str().c_str());
    tmpstream.clear(); tmpstream.str("");

    // draw map
    GwollumPlot::Draw(qplanes[q]->tfmap,"COLZ");

    // save map in ROOT file
    if(aFormat.find("root")!=string::npos){
      tmpstream<<"map"<<q;
      if(qplanes[q]->Omap::Write(frootmap, tmpstream.str())==0)
        cerr<<"Otile::SaveMaps: cannot save map Q"<<q<<endl;
      tmpstream.clear(); tmpstream.str("");
    }
    
    // loop over time windows
    if(fVerbosity>2) cout<<"\t\t- Save windowed maps"<<endl;
    for(unsigned int w=0; w<pwin.size(); w++){
      
      // zoom in
      qplanes[q]->tfmap->GetXaxis()->SetRangeUser((double)SeqT0+aTimeOffset-(double)pwin[w]/2.0,
                                                  (double)SeqT0+aTimeOffset+(double)pwin[w]/2.0);
      
      // loudest tile info
      if(!mapfill.compare("amplitude"))
        tmpstream<<"Loudest: GPS="<<fixed<<setprecision(3)<<qplanes[q]->GetTileTime(t_snrmax[q][w],f_snrmax[q][w])
                 <<", f="<<qplanes[q]->GetBandFrequency(f_snrmax[q][w])<<" Hz, "
                 <<mapfill<<"="<<scientific<<qplanes[q]->GetTileContent(t_snrmax[q][w],f_snrmax[q][w]);
      else
        tmpstream<<"Loudest: GPS="<<fixed<<setprecision(3)<<qplanes[q]->GetTileTime(t_snrmax[q][w],f_snrmax[q][w])
                 <<", f="<<qplanes[q]->GetBandFrequency(f_snrmax[q][w])<<" Hz, "
                 <<mapfill<<"="<<qplanes[q]->GetTileContent(t_snrmax[q][w],f_snrmax[q][w]);
      GwollumPlot::AddText(tmpstream.str(), 0.01,0.01,0.03);
      tmpstream.clear(); tmpstream.str("");
      
      // set Z-axis range
      if(vrange[0]<vrange[1]) qplanes[q]->tfmap->GetZaxis()->SetRangeUser(vrange[0],vrange[1]);
      else qplanes[q]->tfmap->GetZaxis()->UnZoom();
      
      // save plot
      for(unsigned int f=0; f<(int)form.size(); f++){
        if(!form[f].compare("root")) continue;
	tmpstream<<aOutdir<<"/"<<aName<<"MAPQ"<<q<<"-"<<SeqT0<<"-"<<pwin[w]<<"."<<form[f];
	GwollumPlot::Print(tmpstream.str());
	tmpstream.clear(); tmpstream.str("");
	if(aThumb){ //thumbnail
	  tmpstream<<aOutdir<<"/th"<<aName<<"MAPQ"<<q<<"-"<<SeqT0<<"-"<<pwin[w]<<"."<<form[f];
	  GwollumPlot::Print(tmpstream.str(),0.5);
	  tmpstream.clear(); tmpstream.str("");
	}
      }

      // save plot with chirp
      if(GetChirpMass()>0){
        chirp->SetRange((double)SeqT0+aTimeOffset-pwin[w]/2.0, chirp->GetParameter(1)-0.00001);
      
	GwollumPlot::Draw(chirp,"LSAME");
	for(unsigned int f=0; f<form.size(); f++){
          if(!form[f].compare("root")) continue;
          tmpstream<<aOutdir<<"/"<<aName<<"MAPQ"<<q<<"C-"<<SeqT0<<"-"<<pwin[w]<<"."<<form[f];
	  GwollumPlot::Print(tmpstream.str());
	  tmpstream.clear(); tmpstream.str("");
	  if(aThumb){ //thumbnail
	    tmpstream<<aOutdir<<"/th"<<aName<<"MAPQ"<<q<<"C-"<<SeqT0<<"-"<<pwin[w]<<"."<<form[f];
	    GwollumPlot::Print(tmpstream.str(),0.5);
	    tmpstream.clear(); tmpstream.str("");
	  }
	}
	GwollumPlot::UnDraw(chirp);
      }
      
    }
    
    // unzoom
    qplanes[q]->tfmap->GetXaxis()->UnZoom();
    qplanes[q]->ApplyOffset(-(double)SeqT0);
  }

  // save map in ROOT file
  if(aFormat.find("root")!=string::npos){
    frootmap->Close();
  }


  // full map
  if(fVerbosity>1) cout<<"\t- full map"<<endl;
  TH2D* fullmap;
  for(unsigned int w=0; w<pwin.size(); w++){

    // get full map
    fullmap = GetFullMap(pwin[w], aTimeOffset);
    fullmap->SetTitle(aName.c_str());

    // draw map
    GwollumPlot::Draw(fullmap,"COLZ");
        
    // loudest tile
    if(!mapfill.compare("amplitude"))
      tmpstream<<"Loudest: GPS="<<fixed<<setprecision(3)<<(double)SeqT0+qplanes[q_snrmax[w]]->GetTileTime(t_snrmax[q_snrmax[w]][w],f_snrmax[q_snrmax[w]][w])
               <<", f="<<qplanes[q_snrmax[w]]->GetBandFrequency(f_snrmax[q_snrmax[w]][w])<<" Hz, "
               <<mapfill<<"="<<scientific<<qplanes[q_snrmax[w]]->GetTileContent(t_snrmax[q_snrmax[w]][w],f_snrmax[q_snrmax[w]][w]);
    else tmpstream<<"Loudest: GPS="<<fixed<<setprecision(3)<<(double)SeqT0+qplanes[q_snrmax[w]]->GetTileTime(t_snrmax[q_snrmax[w]][w],f_snrmax[q_snrmax[w]][w])
                  <<", f="<<qplanes[q_snrmax[w]]->GetBandFrequency(f_snrmax[q_snrmax[w]][w])<<" Hz, "
                  <<mapfill<<"="<<qplanes[q_snrmax[w]]->GetTileContent(t_snrmax[q_snrmax[w]][w],f_snrmax[q_snrmax[w]][w]);
    GwollumPlot::AddText(tmpstream.str(), 0.01,0.01,0.03);
    tmpstream.clear(); tmpstream.str("");
    
    // set vertical range
    if(vrange[0]<vrange[1]) fullmap->GetZaxis()->SetRangeUser(vrange[0],vrange[1]);
    else fullmap->GetZaxis()->UnZoom();
        
    // save plot
    for(unsigned int f=0; f<(int)form.size(); f++){
      tmpstream<<aOutdir<<"/"<<aName<<"MAP"<<"-"<<SeqT0<<"-"<<pwin[w]<<"."<<form[f];
      GwollumPlot::Print(tmpstream.str());
      tmpstream.clear(); tmpstream.str("");
      if(aThumb){ //thumbnail
	tmpstream<<aOutdir<<"/th"<<aName<<"MAP"<<"-"<<SeqT0<<"-"<<pwin[w]<<"."<<form[f];
	GwollumPlot::Print(tmpstream.str(),0.5);
	tmpstream.clear(); tmpstream.str("");
      }
    }

    // save plot with chirp
    if(GetChirpMass()>0){
      chirp->SetRange((double)SeqT0+aTimeOffset-pwin[w]/2.0, chirp->GetParameter(1)-0.00001);
      GwollumPlot::Draw(chirp,"LSAME");
      for(unsigned int f=0; f<form.size(); f++){
	tmpstream<<aOutdir<<"/"<<aName<<"MAP"<<"C-"<<SeqT0<<"-"<<pwin[w]<<"."<<form[f];
	GwollumPlot::Print(tmpstream.str());
	tmpstream.clear(); tmpstream.str("");
	if(aThumb){ //thumbnail
	  tmpstream<<aOutdir<<"/th"<<aName<<"MAP"<<"C-"<<SeqT0<<"-"<<pwin[w]<<"."<<form[f];
	  GwollumPlot::Print(tmpstream.str(),0.5);
	  tmpstream.clear(); tmpstream.str("");
	}
      }
      
    }
    
    delete fullmap;
  }

  // clean up and exit
  for(unsigned int q=0; q<nq; q++){
    delete t_snrmax[q];
    delete f_snrmax[q];
  }
  delete q_snrmax;
  return snr_max_w0;
}

////////////////////////////////////////////////////////////////////////////////////
vector <double> Otile::ComputeQs(const double aQMin, const double aQMax, const double aMaximumMismatch){
////////////////////////////////////////////////////////////////////////////////////  

  // adjust input parameters
  double qmin=aQMin;
  double qmax=aQMax;
  if(fabs(qmax)<fabs(qmin)){ qmin=fabs(qmax); qmax=fabs(qmin); }
  if(qmin<TMath::Sqrt(11.0)) qmin=TMath::Sqrt(11.0);
  if(qmax<TMath::Sqrt(11.0)) qmax=TMath::Sqrt(11.0);
  if(qmax<qmin){ qmin=qmax; qmax=qmin; }

  // number of planes
  double QCumulativeMismatch = TMath::Log(qmax/qmin)/TMath::Sqrt(2.0);
  double mismatchstep = 2.0*TMath::Sqrt(aMaximumMismatch/3.0);
  int n = (int)ceil(QCumulativeMismatch/mismatchstep);
  if(n<=0) n=1;
  double Qmismatchstep = QCumulativeMismatch/(double)n;
  
  // compute Q values
  vector <double> qs;
  for(int i=0; i<n; i++) qs.push_back(qmin * TMath::Exp(TMath::Sqrt(2.0) * (0.5+i) * Qmismatchstep));
  return qs;
}

////////////////////////////////////////////////////////////////////////////////////
TH2D* Otile::GetFullMap(const unsigned int aTimeRange, const double aTimeOffset){
////////////////////////////////////////////////////////////////////////////////////  

  // time range
  unsigned int trange = TMath::Min(aTimeRange, GetTimeRange());
  
  // create combined tiling
  double *fbins = GetFullMapBands();
  unsigned int ntbins = GetBandTileN(nq, 0);
  TH2D *fullmap = new TH2D("fullmap","Full map",
                           ntbins,
                           SeqT0+aTimeOffset-(double)aTimeRange/2.0,
                           SeqT0+aTimeOffset+(double)aTimeRange/2.0,
                           GetBandN(nq), fbins);
  fullmap->SetDirectory(0);
  delete fbins;
  fullmap->GetXaxis()->SetTitle("Time [s]");
  fullmap->GetYaxis()->SetTitle("Frequency [Hz]");
  fullmap->GetZaxis()->SetTitle(StringToUpper(mapfill).c_str());
  fullmap->GetXaxis()->SetNoExponent();
  fullmap->GetXaxis()->SetNdivisions(4,5,0);
  fullmap->GetXaxis()->SetLabelSize(0.045);
  fullmap->GetYaxis()->SetLabelSize(0.045);
  fullmap->GetZaxis()->SetLabelSize(0.045);
  fullmap->GetXaxis()->SetTitleSize(0.045);
  fullmap->GetYaxis()->SetTitleSize(0.045);
  fullmap->GetZaxis()->SetTitleSize(0.045);
  
  unsigned int tstart, tend, ttstart, ttend, ffstart, ffend;
  double content, snr2;

  // loop over q planes
  for(unsigned int q=0; q<nq; q++){

    // loop over frequency band of that q plane
    for(unsigned int f=0; f<qplanes[q]->GetBandN(); f++){

      // first and last frequency bin of full map to consider
      ffstart=fullmap->GetYaxis()->FindBin(qplanes[q]->GetBandStart(f));
      ffend=fullmap->GetYaxis()->FindBin(qplanes[q]->GetBandEnd(f));

      // qplane time bin indexes to sweep
      tstart=qplanes[q]->GetTimeTileIndex(f,fullmap->GetXaxis()->GetBinLowEdge(1)-SeqT0);
      tend=qplanes[q]->GetTimeTileIndex(f,fullmap->GetXaxis()->GetBinUpEdge(fullmap->GetNbinsX()-1)-SeqT0);

      // loop over relevant qplane time bins
      for(unsigned int t=tstart; t<=tend; t++){

	// first and last time bin of full map to consider
	ttstart=fullmap->GetXaxis()->FindBin(qplanes[q]->GetTileTimeStart(t,f)+SeqT0);
	ttend=fullmap->GetXaxis()->FindBin(qplanes[q]->GetTileTimeEnd(t,f)+SeqT0);
	
	snr2=qplanes[q]->GetTileSnrSq(t,f);
	content=qplanes[q]->GetTileContent(t,f);

	// loop over full map bins overlapping with qplane bins
	for(unsigned int tt=ttstart; tt<=ttend; tt++)
	  for(unsigned int ff=ffstart; ff<=ffend; ff++)
	    if(snr2>fullmap->GetBinError(tt,ff)){// update content if higher SNR
	      fullmap->SetBinContent(tt,ff,content);
	      fullmap->SetBinError(tt,ff,snr2);
	    }
      }
    }
    
  }
  
  return fullmap;
}

////////////////////////////////////////////////////////////////////////////////////
double* Otile::GetFullMapBands(void){
////////////////////////////////////////////////////////////////////////////////////  
  unsigned int nfbins = GetBandN(nq);
  double *fbins = new double [nfbins+1];
  for(unsigned int f=0; f<=nfbins; f++) fbins[f] = GetFullMapFrequencyStart(f);
  return fbins;
}
