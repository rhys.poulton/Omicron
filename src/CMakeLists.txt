# -- dependencies -----------

# FFTW
pkg_check_modules(FFTW REQUIRED "fftw3")

# FrameL
pkg_check_modules(FRAMEL REQUIRED "framel")

# HDF5
find_package(HDF5 COMPONENTS CXX)

# ROOT
find_package(ROOT 6.20 REQUIRED COMPONENTS Core RIO Hist Graf Gpad Tree Matrix MathCore)
message(STATUS "Found ROOT libraries in ${ROOT_LIBRARY_DIR}")
message(STATUS "ROOT libraries: ${ROOT_LIBRARIES}")
list(APPEND CMAKE_PREFIX_PATH $ENV{ROOTSYS})
include(${ROOT_USE_FILE})

# GWOLLUM
pkg_check_modules(GWOLLUM REQUIRED IMPORTED_TARGET "gwollum=${PROJECT_VERSION}")
message(STATUS "GWOLLUM headers: ${GWOLLUM_INCLUDE_DIRS}")
message(STATUS "GWOLLUM libraries: ${GWOLLUM_LIBRARY_DIRS}")
message(STATUS "                   ${GWOLLUM_LIBRARIES}")
message(STATUS "HDF5 libraries: ${HDF5_INCLUDE_DIRS}")

# add link paths
link_directories(
  ${FFTW_LIBRARY_DIRS}
  ${FRAMEL_LIBRARY_DIRS}
  ${GWOLLUM_LIBRARY_DIRS}
)

# -- versioning -----------
configure_file(${CMAKE_CURRENT_LIST_DIR}/Oconfig.h.in
  ${CMAKE_CURRENT_LIST_DIR}/Oconfig.h
  @ONLY)

# -- libOmicron -------------

set(
  OMICRON_HEADERS
  Oconfig.h
  Omap.h
  Oqplane.h
  Otile.h
  Oinject.h
  Oomicron.h
  Ox.h
  )

# build ROOT dictionary
include_directories(
  ${GWOLLUM_INCLUDE_DIRS}
  ${FRAMEL_INCLUDE_DIRS}
  ${HDF5_INCLUDE_DIRS}
  ${FFTW_INCLUDE_DIRS}
  )
root_generate_dictionary(
  OmicronDict
  LINKDEF LinkDef.h
  MODULE Omicron
  OPTIONS ${OMICRON_HEADERS}
  )

# compile library
# NOTE: we call this `libOmicron` here to not clash with the `omicron`
#       executable target below, cmake can get easily confused on
#       case-insensitive file systems
add_library(
  libOmicron
  SHARED
  Omap.cc
  Oqplane.cc
  Otile.cc
  Oinject.cc
  Outils.cc
  Oparameters.cc
  Ohtml.cc
  Oomicron.cc
  Ox.cc
  OmicronDict.cxx
  )
target_include_directories(
  libOmicron
  PUBLIC
  ${GWOLLUM_INCLUDE_DIRS}
  ${CMAKE_CURRENT_SOURCE_DIR}
  )
target_link_libraries(
  libOmicron
  ${ROOT_Core_LIBRARY}
  ${ROOT_Gpad_LIBRARY}
  ${ROOT_Hist_LIBRARY}
  ${ROOT_MathCore_LIBRARY}
  ${ROOT_RIO_LIBRARY}
  ${FFTW_LIBRARIES}
  CUtils
  Inject
  RootUtils
  Segments
  Streams
  Time
  Triggers
  )

set_target_properties(
  libOmicron PROPERTIES
  OUTPUT_NAME Omicron
  PUBLIC_HEADER "${OMICRON_HEADERS}"
  )


# -- libOmicronUtils --------

add_library(
  OmicronUtils
  SHARED
  OmicronUtils.cc
  )
set_target_properties(
  OmicronUtils
  PROPERTIES
  PUBLIC_HEADER "OmicronUtils.h"
  )
target_link_libraries(
  OmicronUtils
  ${ROOT_Core_LIBRARY}
  CUtils
  Streams
  )

# -- executables ------------

add_executable(
  omicron
  omicron.cc
  )
target_link_libraries(
  omicron
  ${ROOT_Core_LIBRARY}
  CUtils
  Segments
  libOmicron
  )

add_executable(
  omicron-x
  omicron-x.cc
  )
target_link_libraries(
  omicron-x
  ${FFTW_LIBRARIES}
  ${ROOT_Core_LIBRARY}
  ${ROOT_RIO_LIBRARY}
  ${ROOT_Hist_LIBRARY}
  ${ROOT_Graf_LIBRARY}
  ${ROOT_Gpad_LIBRARY}
  ${ROOT_MathCore_LIBRARY}
  CUtils
  RootUtils
  Segments
  OmicronUtils
  )

add_executable(
  omicron-listfile
  omicron-listfile.cc
  )
target_link_libraries(
  omicron-listfile
  OmicronUtils
  libOmicron
  )

add_executable(
  omicron-scanfile
  omicron-scanfile.cc
  )
target_link_libraries(
  omicron-scanfile
  ${ROOT_Core_LIBRARY}
  ${ROOT_RIO_LIBRARY}
  CUtils
  OmicronUtils
  )

add_executable(
  omicron-plot
  omicron-plot.cc
  )
target_link_libraries(
  omicron-plot
  ${ROOT_Core_LIBRARY}
  ${ROOT_Graf_LIBRARY}
  ${ROOT_Gpad_LIBRARY}
  ${ROOT_MathCore_LIBRARY}
  CUtils
  RootUtils
  Segments
  Triggers
  OmicronUtils
  )

add_executable(
  omicron-print
  omicron-print.cc
  )
target_link_libraries(
  omicron-print
  ${ROOT_Core_LIBRARY}
  CUtils
  Segments
  Triggers
  OmicronUtils
  )

add_executable(
  omicron-coinc
  omicron-coinc.cc
  )
target_link_libraries(
  omicron-coinc
  ${ROOT_Core_LIBRARY}
  ${ROOT_Graf_LIBRARY}
  ${ROOT_MathCore_LIBRARY}
  CUtils
  RootUtils
  Segments
  Triggers
  Coinc
  OmicronUtils
  )

#add_executable(
#  omicron-metric-print
#  omicron-metric-print.cc
#  )
#target_link_libraries(
#  omicron-metric-print
#  ${ROOT_Core_LIBRARY}
#  ${ROOT_Hist_LIBRARY}
#  CUtils
#  Triggers
#  OmicronUtils
#  )

# -- test executables ------------

add_executable(
  omicron-test-otile
  omicron-test-otile.cc
  )
target_link_libraries(
  omicron-test-otile
  ${ROOT_Core_LIBRARY}
  CUtils
  Segments
  libOmicron
  )
add_test(
  NAME omicron-test-otile
  COMMAND omicron-test-otile
  )

# -- installation -----------


# install library
install(
  TARGETS
  libOmicron
  OmicronUtils
  RUNTIME DESTINATION ${RUNTIME_DESTINATION}
  LIBRARY DESTINATION ${LIBRARY_DESTINATION}
  ARCHIVE DESTINATION ${CMAKE_INSTALL_LIBDIR}
  PUBLIC_HEADER DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}
  )

# install ROOT PCM
install(
  FILES
  ${CMAKE_CURRENT_BINARY_DIR}/libOmicron_rdict.pcm
  ${CMAKE_CURRENT_BINARY_DIR}/libOmicron.rootmap
  DESTINATION ${CMAKE_INSTALL_LIBDIR}
  )

# install executable(s)
install(
  TARGETS
  omicron
  omicron-x
  omicron-listfile
  omicron-scanfile
  omicron-plot
  omicron-print
  omicron-coinc
  #  omicron-metric-print
  DESTINATION ${CMAKE_INSTALL_BINDIR}
  )

# -- pkgconfig --------------

# install pkgconfig file
configure_file(
  "${CMAKE_CURRENT_SOURCE_DIR}/${PROJECT_NAME_LOWER}.pc.in"
  "${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME_LOWER}.pc"
  @ONLY
  )
install(
  FILES
  ${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME_LOWER}.pc
  DESTINATION
  ${CMAKE_INSTALL_LIBDIR}/pkgconfig/
  )
