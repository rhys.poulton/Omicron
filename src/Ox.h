/**
 * @file 
 * @brief Omicron cross-correlation.
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#ifndef __Ox__
#define __Ox__

#include "Oomicron.h"

using namespace std;

/**
 * @brief TBC.
 * @details TBC.
 */
class Ox {

 public:

  /**
   * @name Constructors and destructors
   @{
  */
  /**
   * @brief Constructor of the Ox class.
   * @details Two Omicron objects are initialized using the input parameter files.
   * @warning Constraints on parameters
   * - The two tiling structures must be strictly identical.
   * - There should be one single channel to process for each Omicron object.
   *
   * FFT plans are initialized to perform the cross-correlation.
   * They cover the duration of the spectrograms excluding half the Omicron overlap at both sides.
   * @param[in] aOptionFile1 Omicron option file (first detector).
   * @param[in] aOptionFile2 Omicron option file (second detector).
   */
  Ox(const string aOptionFile1, const string aOptionFile2);

  /**
   * @brief Destructor of the Ox class.
   */
  virtual ~Ox(void);
  /**
     @}
  */

  /**
   * @brief Processes a list of time segments.
   * @details TBC.
   * @param[in] aInSeg List of time segments to process.
   * @pre The list of segments must be valid.
   */
  bool Process(Segments *aInSeg);

  /**
   * @brief Normalizes the cross-correlation maps.
   * @details When this option is active, the cross-correlation map is normalized.
   * For each frequency row, the median of the cross-correlation coefficients is computed.
   * This median value is subtracted from each bins in the frequency row.
   * @param[in] aNormalize Set this flag to true to activate the normalization. Set it to false to keep original cross-correlation maps.
   */
  inline void SetNormalize(const bool aNormalize){ flag_normalize = aNormalize; };

  /**
   * @brief Configures the analysis to only process the full map.
   * @param[in] aOnlyFullMap Set this flag to true to only process the full time-frequency map. Set it to false to also process individual Q planes.
   */
  inline void SetMapAnalysis(const bool aOnlyFullMap){ flag_full_only = aOnlyFullMap; };

private:

  bool status;         ///< Class status.
  Omicron *O1;         ///< Omicron object (1).
  Omicron *O2;         ///< Omicron object (2).

  bool flag_full_only; ///< Flag to only process the full map.
  bool flag_normalize; ///< Flag to normalize the cross-correlation map.
  fft *fft1;           ///< X FFT plan for detector 1.
  fft *fft2;           ///< X FFT plan for detector 2.
  TH2D **h1;           ///< Time-frequency maps for detector 1.
  TH2D **h2;           ///< Time-frequency maps for detector 2.
  TH2D **hx;           ///< Cross-correlation time-frequency maps.

  ClassDef(Ox,0)
};

#endif


