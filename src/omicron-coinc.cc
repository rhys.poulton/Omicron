/**
 * @file 
 * @brief Program to compare two omicron trigger sets.
 * @details 
 * ## Usage
 *
 * `omicron-coinc` is a command line program to compare two trigger sets.
 * A time coincidence between the 2 trigger sets is performed.
 * By default the list of coincident events is printed in the standard output. For example:
 * @verbatim
omicron-coinc channel1=[channel name] channel2=[channel name] gps-start=[GPS start] gps-end=[GPS end]
 @endverbatim
 * This command assumes that omicron trigger root files are saved in a standard place pointed by the environement variable `$OMICRON_TRIGGERS`.
 * It is also possible to use specific trigger files:
 * @verbatim
omicron-coinc file1=[trigger file pattern] file2=[trigger file pattern] gps-start=[GPS start] gps-end=[GPS end]
 @endverbatim
 * where `[trigger file pattern]` can contain wild cards. For example: `file1="/path1/to/triggers/\*.root /path2/to/triggers/\*.root"`.
 *
 * Additionnally, one can print the triggers which are not found in coincidence. For example, for the first trigger set:
 * @verbatim
omicron-coinc channel1=[channel name] channel2=[channel name] gps-start=[GPS start] gps-end=[GPS end] print-list-nocoinc1=1
 @endverbatim
 *
 * It is also possible to print diagnostic plots with the option: `print-plot-coinc=1`.
 *
 * ## Example 1
 *
 * Let's perform the coincidence between 2 channels (`V1:LSC_DARM` and `V1:Hrec_hoft_16384Hz`) over 6 hours of data:
 * @verbatim
omicron-coinc channel1=V1:LSC_DARM channel2=V1:Hrec_hoft_16384Hz gps-start=1266969618 gps-end=1266991218

*********************************************************************
coinc_0
  V1:LSC_DARM: t=1266970234.90625 s, f=3.34949 Hz, snr=9.01751
  V1:Hrec_hoft_16384Hz: t=1266970234.95312 s, f=149.80423, snr=8.09652
*********************************************************************
...
*********************************************************************
coinc_15
  V1:LSC_DARM: t=1266988209.89355 s, f=1887.11766 Hz, snr=7.54301
  V1:Hrec_hoft_16384Hz: t=1266988209.89160 s, f=1914.72933, snr=7.78166
*********************************************************************
Valid livetime                         = 21592.00000 s
Coincident livetime                    = 21592.00000 s
Number of valid clusters for channel 1 = 78
Number of valid clusters for channel 2 = 91
Number of coinc clusters               = 16
 @endverbatim
 * In this example, 16 clusters are found in coincidence out of 78 for `V1:LSC_DARM` and out of 91 for `V1:Hrec_hoft_16384Hz`. The parameters for these events are printed for both channels.
 *
 * ## Example 2
 *
 * Let's perform the coincidence between 2 channels (`V1:LSC_DARM` and `V1:Hrec_hoft_16384Hz`) over 6 hours of data:
 * @verbatim
omicron-coinc channel1=V1:LSC_DARM channel2=V1:Hrec_hoft_16384Hz gps-start=1266969618 gps-end=1266991218 print-list-coinc=0 print-list-nocoinc1=1

V1:LSC_DARM: t=1266970228.17969 s, f=346.09037 Hz, snr=7.00438
V1:LSC_DARM: t=1266970327.68750 s, f=7.95789 Hz, snr=7.72376
...
V1:LSC_DARM: t=1266989603.15137 s, f=1585.80320 Hz, snr=7.80582
V1:LSC_DARM: t=1266990803.54688 s, f=12.36363 Hz, snr=8.17653
Valid livetime                         = 21592.00000 s
Coincident livetime                    = 21592.00000 s
Number of valid clusters for channel 1 = 78
Number of valid clusters for channel 2 = 91
Number of coinc clusters               = 16
 @endverbatim
 * In this example, 78-16=62 `V1:LSC_DARM` clusters are not found in coincidence with `V1:Hrec_hoft_16384Hz` clusters. The parameters of these events are printed. 
 *
 * ## Example 3
 *
 * Let's perform the coincidence between 2 channels (`V1:LSC_DARM` and `V1:Hrec_hoft_16384Hz`) over 6 hours of data:
 * @verbatim
omicron-coinc channel1=V1:LSC_DARM channel2=V1:Hrec_hoft_16384Hz gps-start=1266969618 gps-end=1266991218 print-list-coinc=0 print-plot-coinc=1

Number of valid clusters for channel 1 = 78
Number of valid clusters for channel 2 = 91
Number of coinc clusters               = 16
 @endverbatim
 * In this example, no events are printed. Instead, with the option `print-plot-coinc=1`, diagnostic plots are produced in the current directory.
 * \anchor omicron-coinc_freqfreq
 * \image html omicron-coinc_freqfreq.png "Frequency-frequency plot for coincident events." width=400
 * \anchor omicron-coinc_snrsnr
 * \image html omicron-coinc_snrsnr.png "SNR-SNR plot for coincident events." width=400
 * \anchor omicron-coinc_snrtime0
 * \image html omicron-coinc_snrtime0.png "SNR-time distribution of V1:LSC_DARM clusters in blue. Coincident events are plotted with red crosses." width=400
 * \anchor omicron-coinc_freqtime0
 * \image html omicron-coinc_freqtime0.png "Frequency-time distribution of V1:LSC_DARM clusters in blue. Coincident events are plotted with red crosses." width=400
 * \anchor omicron-coinc_snrfrac0
 * \image html omicron-coinc_snrfrac0.png "Fraction of coincident events for V1:LSC_DARM as a function of SNR." width=400
 * \anchor omicron-coinc_freqfrac0
 * \image html omicron-coinc_freqfrac0.png "Fraction of coincident events for V1:LSC_DARM as a function of frequency." width=400
 * \anchor omicron-coinc_snrtime1
 * \image html omicron-coinc_snrtime1.png "SNR-time distribution of V1:Hrec_hoft_16384Hz clusters in blue. Coincident events are plotted with red crosses." width=400
 * \anchor omicron-coinc_freqtime1
 * \image html omicron-coinc_freqtime1.png "Frequency-time distribution of V1:Hrec_hoft_16384Hz clusters in blue. Coincident events are plotted with red crosses." width=400
 * \anchor omicron-coinc_snrfrac1
 * \image html omicron-coinc_snrfrac1.png "Fraction of coincident events for V1:Hrec_hoft_16384Hz as a function of SNR." width=400
 * \anchor omicron-coinc_freqfrac1
 * \image html omicron-coinc_freqfrac1.png "Fraction of coincident events for V1:Hrec_hoft_16384Hz as a function of frequency." width=400
 *
 *
 * ## Options
 *
 * The `omicron-coinc` command comes with many additional options. Type `omicron-coinc` to get the full list of options.
 * @snippet this omicron-coinc-usage 
 *
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#include <ReadTriggers.h>
#include <Coinc2.h>
#include "OmicronUtils.h"

using namespace std;

/**
 * @brief Print the program usage message.
 */
void PrintUsage(void){
  //! [omicron-coinc-usage]
  cerr<<endl;
  cerr<<"Usage:"<<endl;
  cerr<<endl;
  cerr<<"omicron-coinc channel1=[channel name] \\"<<endl;
  cerr<<"              channel2=[channel name] \\"<<endl;
  cerr<<"              file1=[trigger file pattern] \\"<<endl;
  cerr<<"              file2=[trigger file pattern] \\"<<endl;
  cerr<<"              gps-start=[GPS start] \\"<<endl;
  cerr<<"              gps-end=[GPS end] \\"<<endl;
  cerr<<"              snr-min1=[minimum SNR] \\"<<endl;
  cerr<<"              snr-min2=[minimum SNR] \\"<<endl;
  cerr<<"              cluster-dt=[cluster time window] \\"<<endl;
  cerr<<"              coinc-dt=[coincidence time window] \\"<<endl;
  cerr<<"              print-list-coinc=[1/0] \\"<<endl;
  cerr<<"              print-list-nocoinc1=[1/0] \\"<<endl;
  cerr<<"              print-list-nocoinc2=[1/0] \\"<<endl;
  cerr<<"              print-plot-coinc=[1/0]"<<endl;
  cerr<<endl;
  cerr<<"[channel name]            channel name used to retrieve centralized Omicron triggers"<<endl;
  cerr<<"[trigger file pattern]    file pattern to ROOT trigger files (GWOLLUM convention)"<<endl;
  cerr<<"[GPS start]               starting GPS time (integer only)"<<endl;
  cerr<<"[GPS end]                 stopping GPS time (integer only)"<<endl;
  cerr<<"[minimum SNR]             minimum SNR value. Default = 7.0"<<endl;
  cerr<<"[cluster time window]     cluster time window [s]. By default, cluster-dt=0.1"<<endl;
  cerr<<"[coincidence time window] coincidence time window [s]. By default, cluster-dt is used"<<endl;
  cerr<<"[1/0]                     1=YES, 0=NO"<<endl;
  cerr<<endl;
  cerr<<"See also: https://virgo.docs.ligo.org/virgoapp/Omicron/omicron-coinc_8cc.html#details"<<endl;
  cerr<<endl;
  //! [omicron-coinc-usage]
  return;
}

/**
 * @brief Main program.
 */
int main (int argc, char* argv[]){

  if(argc>1&&!((string)argv[1]).compare("version")){
    return OmicronPrintVersion();
  }

  // number of arguments
  if(argc<2){
    PrintUsage();
    return 1;
  }

  // list of parameters + default
  string chname1="";     // channel1 name
  string chname2="";     // channel2 name
  string tfile_pat1="";  // trigger file pattern 1
  string tfile_pat2="";  // trigger file pattern 2
  unsigned int gps_start=0;// GPS start
  unsigned int gps_end=0;// GPS end
  double snr_min1=7.0;   // SNR min 1
  double snr_min2=7.0;   // SNR min 2
  double cluster_dt=0.1; // cluster time window
  double coinc_dt=-1.0;  // coinc time window
  bool print_list_coinc=true;// flag to print coinc events
  bool print_list_nocoinc1=false;// flag to print not coinc events (1)
  bool print_list_nocoinc2=false;// flag to print not coinc events (2)
  bool print_plot_coinc=false;// flag to print coinc plots

  // loop over arguments
  vector <string> sarg;
  for(int a=1; a<argc; a++){
    sarg=SplitString((string)argv[a],'=');
    if(sarg.size()!=2) continue;
    if(!sarg[0].compare("channel1"))       chname1=(string)sarg[1];
    if(!sarg[0].compare("channel2"))       chname2=(string)sarg[1];
    if(!sarg[0].compare("file1"))          tfile_pat1=sarg[1];
    if(!sarg[0].compare("file2"))          tfile_pat2=sarg[1];
    if(!sarg[0].compare("gps-start"))      gps_start=stoul(sarg[1].c_str());
    if(!sarg[0].compare("gps-end"))        gps_end=stoul(sarg[1].c_str());
    if(!sarg[0].compare("snr-min1"))       snr_min1=atof(sarg[1].c_str());
    if(!sarg[0].compare("snr-min2"))       snr_min2=atof(sarg[1].c_str());
    if(!sarg[0].compare("cluster-dt"))     cluster_dt=atof(sarg[1].c_str());
    if(!sarg[0].compare("coinc-dt"))       coinc_dt=atof(sarg[1].c_str());
    if(!sarg[0].compare("print-list-coinc")) print_list_coinc=(bool)atoi(sarg[1].c_str());
    if(!sarg[0].compare("print-list-nocoinc1")) print_list_nocoinc1=(bool)atoi(sarg[1].c_str());
    if(!sarg[0].compare("print-list-nocoinc2")) print_list_nocoinc2=(bool)atoi(sarg[1].c_str());
    if(!sarg[0].compare("print-plot-coinc")) print_plot_coinc=(bool)atoi(sarg[1].c_str());
  }

  // centralized trigger files
  if(!tfile_pat1.compare("")){
    if(chname1.compare("")&&gps_start>0&&gps_end>0) tfile_pat1=GetOmicronFilePattern(chname1,gps_start,gps_end);
    else{
      cerr<<"Triggers (1) must be specified, either with a channel name and a time range or with a file pattern"<<endl;
      cerr<<"Type omicron-coinc for help"<<endl;
      return 1;
    }
  }
  // check file pattern
  if(!tfile_pat1.compare("")){
    cerr<<"No trigger files (1)"<<endl;
    return 2;
  }
  
  // centralized trigger files
  if(!tfile_pat2.compare("")){
    if(chname2.compare("")&&gps_start>0&&gps_end>0) tfile_pat2=GetOmicronFilePattern(chname2,gps_start,gps_end);
    else{
      cerr<<"Triggers (2) must be specified, either with a channel name and a time range or with a file pattern"<<endl;
      cerr<<"Type omicron-coinc for help"<<endl;
      return 3;
    }
  }
  // check file pattern
  if(!tfile_pat2.compare("")){
    cerr<<"No trigger files (2)"<<endl;
    return 4;
  }

  // triggers
  ReadTriggers *RT1 = new ReadTriggers(tfile_pat1,"",0);
  if(!RT1->GetStatus()||!RT1->GetLiveTime()) return 5;
  ReadTriggers *RT2 = new ReadTriggers(tfile_pat2,"",0);
  if(!RT2->GetStatus()||!RT2->GetLiveTime()) return 5;

  // clustering
  RT1->SetClusterizeDt(cluster_dt);
  RT1->SetClusterizeSnrThr(snr_min1);
  RT1->Clusterize(1);
  RT2->SetClusterizeDt(cluster_dt);
  RT2->SetClusterizeSnrThr(snr_min2);
  RT2->Clusterize(1);

  // valid segments
  if(gps_start==0) gps_start=TMath::Min(RT1->Segments::GetFirst(), RT2->Segments::GetLast());
  if(gps_end==0) gps_end=TMath::Max(RT1->Segments::GetFirst(), RT2->Segments::GetLast());
  Segments *seg = new Segments(gps_start, gps_end);
  seg->Intersect(RT1);
  seg->Intersect(RT2);

  // coinc
  Coinc2 *Co = new Coinc2(0);
  Co->SetTriggers(RT1, RT2, 0.0, 0.0, seg);
  if(coinc_dt<0) coinc_dt = cluster_dt;
  Co->SetCoincDeltat(coinc_dt);
  unsigned int ncoinc = Co->MakeCoinc();
  
  // print
  if(print_list_nocoinc1) Co->PrintCoincNot(0);
  if(print_list_nocoinc2) Co->PrintCoincNot(1);
  if(print_list_coinc)    Co->PrintCoinc();
  if(print_plot_coinc){
    Co->MakeComparators();
    Co->PrintComparators("./omicron-coinc");
  }
  
  // print a few numbers
  cout<<"Valid livetime                         = "<<seg->GetLiveTime()<<" s"<<endl;
  cout<<"Coincident livetime                    = "<<Co->GetCoincLiveTime()<<" s"<<endl;
  cout<<"Number of valid clusters for channel 1 = "<<Co->GetActiveClusterN(0)<<endl;
  cout<<"Number of valid clusters for channel 2 = "<<Co->GetActiveClusterN(1)<<endl;
  cout<<"Number of coinc clusters               = "<<Co->GetCoincN()<<endl;

  // cleaning
  delete Co;
  delete seg;
  delete RT1;
  delete RT2;

  return 0;
}
