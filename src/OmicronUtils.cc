/**
 * @file 
 * @brief See OmicronUtils.h
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#include "OmicronUtils.h"

static string GetFiles(unsigned int &aEffectiveTimeEnd, const string aDir,
                       const unsigned int aTimeStart, const unsigned int aTimeEnd);

////////////////////////////////////////////////////////////////////////////////////
string GetOmicronFilePattern(const string aChannelName,
                             const unsigned int aTimeStart, const unsigned int aTimeEnd){
////////////////////////////////////////////////////////////////////////////////////

  // trigger directories
  char *stmp = getenv("OMICRON_TRIGGERS");
  string trigdir;
  if(stmp==NULL) trigdir=".";
  else trigdir = (string)stmp;
  stmp = getenv("OMICRON_TRIGGERS_ONLINE");
  string onlinedir;
  if(stmp==NULL) onlinedir=".";
  else onlinedir = (string)stmp;

  string filelist="";
  unsigned int effend = 0;
  
  // special case of hpss
  if(trigdir.find("hpss")!=string::npos)
    return GetOmicronFilePatternFromHpss(aChannelName, aTimeStart, aTimeEnd);
    
  // channel stream
  Streams *S = new Streams(aChannelName, 0);
  
  // LIGO-Virgo OFFLINE directory
  stringstream lv_dir;
  lv_dir<< trigdir << "/" << S->GetNamePrefix() << "/" << S->GetNameSuffixUnderScore() << "_OMICRON";
  filelist += GetFiles(effend, lv_dir.str(), aTimeStart, aTimeEnd);
  lv_dir.clear(); lv_dir.str("");

  // LIGO-Virgo ONLINE directory
  // (starting where we stopped in the offline directory)
  lv_dir<< onlinedir << "/" << S->GetNamePrefix() << "/" << S->GetNameSuffixUnderScore() << "_OMICRON";
  filelist+=GetFiles(effend, lv_dir.str(), TMath::Max(effend, aTimeStart), aTimeEnd);
  lv_dir.clear(); lv_dir.str("");

  delete S;
  return filelist;
}

////////////////////////////////////////////////////////////////////////////////////
string GetOmicronFilePatternFromHpss(const string aChannelName,
                                     const unsigned int aTimeStart, const unsigned int aTimeEnd){
////////////////////////////////////////////////////////////////////////////////////
  string filelist="";

  // trigger directory
  string trigdir = getenv("OMICRON_TRIGGERS");

  // channel stream
  Streams *S = new Streams(aChannelName,0);

  // random id
  srand(time(NULL));
  int randid = rand();

  // tmp file
  stringstream ss;
  ss<<getenv("TMP")<<"/omf-"<<S->GetNameSuffixUnderScore()<<"-"<<aTimeStart<<"-"<<aTimeEnd<<"."<<randid<<".tmp";
  string tmpfile=ss.str();
  ss.clear(); ss.str("");

  // rfdir command
  ss<<"rfdir ${OMICRON_TRIGGERS}/"<<S->GetNamePrefix()<<"/"<<S->GetNameSuffixUnderScore()<<"_OMICRON | grep -v \"\\.\" | awk '{print $9}' | sort -u> "<<tmpfile;

  // dump list of channel sub-directories in a tmp file
  if(system(ss.str().c_str())!=0) return filelist;
  ss.clear(); ss.str("");

  ReadAscii *R = new ReadAscii(tmpfile,"u");
  if(!R->GetNRow()) { delete R; return filelist; }

  // list of gps directories
  unsigned int stoproot = aTimeEnd / 100000;
  vector <unsigned int> gpsdir; unsigned int gps;
  for(unsigned int g=0; g<R->GetNRow(); g++){
    R->GetElement(gps,g,0);
    if(gps<=stoproot) gpsdir.push_back(gps);
  }

  delete R;
  
  // list relevant files in gps directories
  vector <string> vfilefrag; string filename;
  unsigned int fstart, fstop;
  for(unsigned int g=0; g<gpsdir.size(); g++){
    ss<<"rfdir ${OMICRON_TRIGGERS}/"<<S->GetNamePrefix()<<"/"<<S->GetNameSuffixUnderScore()<<"_OMICRON/"<<gpsdir[g]<<" | grep OMICRON | awk '{print $9}' | sort -u> "<<tmpfile;
    if(system(ss.str().c_str())!=0) return "";
    ss.clear(); ss.str("");

    R = new ReadAscii(tmpfile,"s");

    // select relevant files in that gps directories
    for(unsigned int gg=0; gg<R->GetNRow(); gg++){
      R->GetElement(filename,gg,0);
     
      // get file fragments
      vfilefrag.clear();
      vfilefrag = SplitString(filename,'-');
    
      // check file naming convention (4 fragments)
      if(vfilefrag.size()!=4) continue;
    
      // file timing
      fstart = atoi(vfilefrag[2].c_str());
      fstop = fstart+atoi(vfilefrag[3].substr(0,vfilefrag[3].size()-5).c_str());
    
      // check file start time
      if(fstart>aTimeEnd) continue;
    
      // check file stop time
      if(fstop<aTimeStart) continue;

      // full file name
      ss<<"root://ccxroot:1999/${OMICRON_TRIGGERS}/"<<S->GetNamePrefix()<<"/"+S->GetNameSuffixUnderScore()<<"_OMICRON/"<<gpsdir[g]<<"/"<<filename;

      // select file
      filelist+=ss.str()+" ";
      ss.clear(); ss.str("");

    }

    if(system(("rm -f "+tmpfile).c_str())!=0)
      cerr<<"GetOmicronFilePatternFromHpss: clean-up failed"<<endl;

    delete R;

  }

  return filelist;
}

/**
 * @brief Returns the list of Omicron trigger files.
 * @param[out] aDir aEffectiveTimeEnd Effective GPS end time found in the file list.
 * @param[in] aDir Directory.
 * @param[in] aDir aTimeStart GPS start time. 
 * @param[in] aDir aTimeEnd GPS start time. 
 */
static string GetFiles(unsigned int &aEffectiveTimeEnd, const string aDir,
                       const unsigned int aTimeStart, const unsigned int aTimeEnd){

  if(!IsDirectory(aDir)) return "";
  
  aEffectiveTimeEnd = aTimeStart;
  string filelist="";
  string filelist_tmp="";
  unsigned int g_start = aTimeStart/100000;
  unsigned int g_stop = aTimeEnd/100000;
  vector <string> timedir;
  vector <string> vfiles;
  vector <string> vfilefrag;
  unsigned int fstart, fstop;
  unsigned int nfiles = 0;

  // get list of time directories
  timedir = Glob((aDir+"/*").c_str());
  if(timedir.size()==0) return "";
  std::sort(timedir.begin(), timedir.end());

  // loop over ALL time directories and preselect 
  for(unsigned t=0; t<timedir.size(); t++){

    // get list of files
    vfiles=Glob((timedir[t]+"*.root").c_str());
    filelist_tmp="";
    nfiles=0;
    
    // loop over files
    for(unsigned int v=0; v<vfiles.size(); v++){
    
      // get file fragments
      vfilefrag.clear();
      vfilefrag = SplitString(GetFileNameFromPath(vfiles[v]),'-');
    
      // check file naming convention (4 fragments)
      if(vfilefrag.size()!=4) continue;
      
      // file start
      fstart = stoul(vfilefrag[2].c_str());
      if(fstart>aTimeEnd) break;

      // file stop
      fstop = fstart+stoul(vfilefrag[3].substr(0,vfilefrag[3].size()-5).c_str());
      if(fstop>aEffectiveTimeEnd) aEffectiveTimeEnd = fstop;
      if(fstop<aTimeStart) continue;
    
      // select file
      filelist_tmp += vfiles[v] + " ";
      nfiles++;
    }

    // all the files in the directory?
    if((nfiles>0) && (nfiles==vfiles.size())) filelist += timedir[t]+"*.root ";
    else filelist += filelist_tmp;
    vfiles.clear();
  }

  return filelist;
}
