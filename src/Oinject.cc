/**
 * @file 
 * @brief See Oinject.h
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#include "Oinject.h"

ClassImp(Oinject)

////////////////////////////////////////////////////////////////////////////////////
Oinject::Oinject(const double aDuration): duration(aDuration) { 
////////////////////////////////////////////////////////////////////////////////////
  
  tau     = 0.0;
  taumin  = 0.0;
  taumax  = 0.0;
  phi     = 0.0;
  phimin  = 32.0;
  phimax  = 512.0;
  Q       = 0.0;
  Qmin    = 4.0;
  Qmax    = 100.0;
  amp     = 1.0e-21;
  ampmin  = 1.0e-21;
  ampmax  = 1.0e-21;
  snr     = 0.0;
  phase   = 0.0;

  Wg      = 0.0;
  sigma_t = 0.0;
  sigma_f = 0.0;
  
  // random generator
  randgen = new TRandom3(0);
  MakeWaveform();
  
}

////////////////////////////////////////////////////////////////////////////////////
Oinject::~Oinject(void){
////////////////////////////////////////////////////////////////////////////////////
  delete randgen;
}

////////////////////////////////////////////////////////////////////////////////////
void Oinject::MakeWaveform(void){
////////////////////////////////////////////////////////////////////////////////////

  // waveform parameters
  GenerateParameters();

  // derived parameters
  Wg      = sqrt(sqrt(2.0/TMath::Pi())*Q/phi);
  sigma_t = Q/phi/TMath::Pi()/sqrt(8.0);
  sigma_f = sqrt(2.0)*phi/Q;

  return;
}

////////////////////////////////////////////////////////////////////////////////////
double Oinject::GetTrueSNR(Spectrum *aSpec1, Spectrum *aSpec2){
////////////////////////////////////////////////////////////////////////////////////

  double freq, win, sum=0;
  double dfreq=aSpec1->GetSpectrumResolution();

  // only positive frequencies. No negative frequency contribution w=0.
  for(unsigned int i=1; i<aSpec1->GetSpectrumSize(); i++){
    freq=aSpec1->GetSpectrumFrequency(i);
    win = Wg * exp(-(freq-phi)*(freq-phi)/2.0/sigma_f/sigma_f);
    sum += win*win/2.0 / sqrt(aSpec1->GetPower(freq)*aSpec2->GetPower(freq)/4.0) * dfreq;
  }

  return amp*sum;
}

////////////////////////////////////////////////////////////////////////////////////
void Oinject::GenerateParameters(void){
////////////////////////////////////////////////////////////////////////////////////

  tau   = randgen->Uniform(taumin, taumax);
  phase = randgen->Uniform(0, 2*TMath::Pi());
  amp   = ampmin*pow(ampmax/ampmin, randgen->Uniform());
  phi   = randgen->Uniform(phimin, phimax);
  Q     = randgen->Uniform(Qmin, Qmax);


}
