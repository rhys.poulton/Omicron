/**
 * @file 
 * @brief See Oomicron.h
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#include "Oomicron.h"

/**
 * @brief html color codes. 
 */
static const string colorcode[17] = {"#1b02e6","#0220e6","#0257e6","#028ee6","#01c5e6","#01e6cf","#01e698","#01e660","#01e629","#10e601","#7fe601","#b6e601","#e6de00","#e6a600","#e66f00","#e63800","#e60000"};

////////////////////////////////////////////////////////////////////////////////////
void Omicron::PrintMessage(const string aMessage){
////////////////////////////////////////////////////////////////////////////////////
  time ( &timer );
  ptm = gmtime ( &timer );
  cout<<">>>>>>>>>> ("<<setfill('0')<<setw(2)<<ptm->tm_hour<<":"
      <<setfill('0')<<setw(2)<<ptm->tm_min<<":"
      <<setfill('0')<<setw(2)<<ptm->tm_sec<<" UTC) [+"<<timer-timer_start<<"s]: "
      <<aMessage<<" <<<<<<<<<<"<<endl;
  return;
}

////////////////////////////////////////////////////////////////////////////////////
void Omicron::PrintStatusInfo(void){
////////////////////////////////////////////////////////////////////////////////////
  if(!status_OK) return;
  if(!inSegments->GetN()) return;
  
  cout<<"\n************* Omicron status info *************"<<endl;
  cout<<"requested start         = "<<(int)inSegments->GetFirst()<<endl;
  cout<<"requested end           = "<<(int)inSegments->GetLast()<<endl;
  cout<<"requested livetime      = "<<(int)inSegments->GetLiveTime()<<" s"<<endl;
  cout<<"number of loaded chunks = "<<chunk_ctr<<endl;

  for(unsigned int c=0; c<nchannels; c++){
    cout<<"\n*** "<<triggers[c]->GetName()<<endl;
    cout<<"number of calls              = "<<chan_ctr[c]<<endl;
    cout<<"number of data calls         = "<<chan_data_ctr[c]<<endl;
    cout<<"number of conditioning calls = "<<chan_cond_ctr[c]<<endl;
    cout<<"number of projection calls   = "<<chan_proj_ctr[c]<<endl;
    cout<<"number of write calls        = "<<chan_write_ctr[c]<<endl;
    if(outSegments[c]->GetN()){
      cout<<"start_out                    = "<<(int)outSegments[c]->GetFirst()<<endl;
      cout<<"end_out                      = "<<(int)outSegments[c]->GetLast()<<endl;
      cout<<"trigger livetime             = "<<(int)outSegments[c]->GetLiveTime()<<"s ("<<outSegments[c]->GetLiveTime()/inSegments->GetLiveTime()*100<<"%)"<<endl;
    }
  }
  cout<<"***********************************************\n"<<endl;

  return;
}

////////////////////////////////////////////////////////////////////////////////////
bool Omicron::IsFlat(const unsigned int aInVectSize, double *aInVect){
////////////////////////////////////////////////////////////////////////////////////
  double vals = aInVect[0];
  double vale = aInVect[aInVectSize-1];
  if(aInVect[0]==aInVect[aInVectSize-1]){
    for(unsigned int i=0; i<aInVectSize; i++)
      if(aInVect[i]!=vals) return false;
    return true;
  }
  
  return false;
}

////////////////////////////////////////////////////////////////////////////////////
string Omicron::GetColorCode(const double aSnrRatio){
////////////////////////////////////////////////////////////////////////////////////
  if(aSnrRatio<0) return "";
  double inc = 0.1;

  if(aSnrRatio>=17.0*inc) return colorcode[16];// saturation

  return colorcode[((unsigned int)floor(aSnrRatio/inc))%17];
}

////////////////////////////////////////////////////////////////////////////////////
vector <string> Omicron::GetChannels(void){
////////////////////////////////////////////////////////////////////////////////////
  vector <string> chanlist;
  for(unsigned int c=0; c<nchannels; c++) chanlist.push_back(triggers[c]->GetName());
  return chanlist;
}

////////////////////////////////////////////////////////////////////////////////////
void Omicron::SaveSG(void){
////////////////////////////////////////////////////////////////////////////////////
  if(oinjfile.is_open()){
    oinjfile<<fixed<<(double)tile->GetChunkTimeCenter()+oinj->GetTime()<<" ";
    oinjfile<<fixed<<oinj->GetFrequency()<<" ";
    oinjfile<<fixed<<oinj->GetQ()<<" ";
    oinjfile<<scientific<<oinj->GetAmplitude()<<" ";
    oinjfile<<fixed<<oinj->GetPhase()<<" ";
    oinjfile<<fixed<<oinj->GetTrueSNR(spectrum1[chanindex], spectrum2[chanindex])<<" ";
    oinjfile<<fixed<<oinj->GetSigmat()<<" ";
    oinjfile<<fixed<<oinj->GetSigmaf()<<" ";
    oinjfile<<endl;
  }
  return;
}

////////////////////////////////////////////////////////////////////////////////////
void Omicron::SaveSummary(void){
////////////////////////////////////////////////////////////////////////////////////
  osummaryfile.open((maindir+"/summary.txt").c_str());

  osummaryfile<<"# requested GPS start # requested GPS end # requested livetime [s] # number of loaded chunks (analysis window)"<<endl;
  osummaryfile<<(unsigned int)inSegments->GetFirst()<<" "<<(unsigned int)inSegments->GetLast()<<" "<<(unsigned int)inSegments->GetLiveTime()<<" "<<chunk_ctr<<endl;
  osummaryfile<<endl;
  
  osummaryfile<<"# channel name # load # data # condition # projection # write # processed livetime [s]"<<endl;
  for(unsigned int c=0; c<nchannels; c++)
    osummaryfile<<triggers[c]->GetName()<<" "<<chan_ctr[c]<<" "<<chan_data_ctr[c]<<" "<<chan_cond_ctr[c]<<" "<<chan_proj_ctr[c]<<" "<<chan_write_ctr[c]<<" "<<outSegments[c]->GetLiveTime()<<endl;

  osummaryfile.close();

  return;
}

////////////////////////////////////////////////////////////////////////////////////
void Omicron::SaveAPSD(const string aType){
////////////////////////////////////////////////////////////////////////////////////

  // averaging factor
  double factor = 2.0;

  // plots
  stringstream ss;
  TGraph *GAPSD1;
  TGraph *GAPSD2;
  TGraph **G1, **G2; // periodograms
  G1 = new TGraph* [spectrum1[chanindex]->GetNSubSegmentsMax(0)+spectrum1[chanindex]->GetNSubSegmentsMax(1)];
  G2 = new TGraph* [spectrum2[chanindex]->GetNSubSegmentsMax(0)+spectrum2[chanindex]->GetNSubSegmentsMax(1)];

  // ASD
  if(!aType.compare("ASD")){

    // averaging factor
    factor = TMath::Sqrt(factor);

    // first whitening
    GAPSD1 = spectrum1[chanindex]->GetASD(tile->GetFrequencyMin(),tile->GetFrequencyMax());
    GAPSD1->SetName("ASD_1");
    for(unsigned int i=0; i<spectrum1[chanindex]->GetNSubSegmentsMax(0); i++){
      ss<<"periodogram1_0_"<<i;
      G1[i] = spectrum1[chanindex]->GetAmplitudePeriodogram(0,i);
      G1[i]->SetName(ss.str().c_str());
      ss.str(""); ss.clear();
    }
    for(unsigned int i=0; i<spectrum1[chanindex]->GetNSubSegmentsMax(1); i++){
      ss<<"periodogram1_1_"<<i;
      G1[spectrum1[chanindex]->GetNSubSegmentsMax(0)+i] = spectrum1[chanindex]->GetAmplitudePeriodogram(1,i);
      G1[spectrum1[chanindex]->GetNSubSegmentsMax(0)+i]->SetName(ss.str().c_str());
      ss.str(""); ss.clear();
    }

    // second whitening
    GAPSD2 = spectrum2[chanindex]->GetASD(tile->GetFrequencyMin(),tile->GetFrequencyMax());
    GAPSD1->SetName("ASD_2");
    for(unsigned int i=0; i<spectrum2[chanindex]->GetNSubSegmentsMax(0); i++){
      ss<<"periodogram2_0_"<<i;
      G2[i] = spectrum2[chanindex]->GetAmplitudePeriodogram(0,i);
      G2[i]->SetName(ss.str().c_str());
      ss.str(""); ss.clear();
    }
    for(unsigned int i=0; i<spectrum2[chanindex]->GetNSubSegmentsMax(1); i++){
      ss<<"periodogram2_1_"<<i;
      G2[spectrum2[chanindex]->GetNSubSegmentsMax(0)+i] = spectrum2[chanindex]->GetAmplitudePeriodogram(1,i);
      G2[spectrum2[chanindex]->GetNSubSegmentsMax(0)+i]->SetName(ss.str().c_str());
      ss.str(""); ss.clear();
    }
  }

  //PSD
  else{

    // first whitening
    GAPSD1 = spectrum1[chanindex]->GetPSD(tile->GetFrequencyMin(),tile->GetFrequencyMax());
    GAPSD1->SetName("PSD_1");
    for(unsigned int i=0; i<spectrum1[chanindex]->GetNSubSegmentsMax(0); i++){
      ss<<"periodogram1_0_"<<i;
      G1[i] = spectrum1[chanindex]->GetPeriodogram(0,i);
      G1[i]->SetName(ss.str().c_str());
      ss.str(""); ss.clear();
    }
    for(unsigned int i=0; i<spectrum1[chanindex]->GetNSubSegmentsMax(1); i++){
      ss<<"periodogram1_1_"<<i;
      G1[spectrum1[chanindex]->GetNSubSegmentsMax(0)+i] = spectrum1[chanindex]->GetPeriodogram(1,i);
      G1[spectrum1[chanindex]->GetNSubSegmentsMax(0)+i]->SetName(ss.str().c_str());
      ss.str(""); ss.clear();
    }

    // second whitening
    GAPSD2 = spectrum2[chanindex]->GetPSD(tile->GetFrequencyMin(),tile->GetFrequencyMax());
    GAPSD1->SetName("PSD_2");
    for(unsigned int i=0; i<spectrum2[chanindex]->GetNSubSegmentsMax(0); i++){
      ss<<"periodogram2_0_"<<i;
      G2[i] = spectrum2[chanindex]->GetPeriodogram(0,i);
      G2[i]->SetName(ss.str().c_str());
      ss.str(""); ss.clear();
    }
    for(unsigned int i=0; i<spectrum2[chanindex]->GetNSubSegmentsMax(1); i++){
      ss<<"periodogram2_1_"<<i;
      G2[spectrum2[chanindex]->GetNSubSegmentsMax(0)+i] = spectrum2[chanindex]->GetPeriodogram(1,i);
      G2[spectrum2[chanindex]->GetNSubSegmentsMax(0)+i]->SetName(ss.str().c_str());
      ss.str(""); ss.clear();
    }
  }
  
  // combine the 2 A/PSD
  for(unsigned int i=0; i<GAPSD2->GetN(); i++)
    GAPSD2->GetY()[i] *= (GAPSD1->GetY()[i]/factor);
   
  // combine the 2 periodograms
  for(unsigned int i=0; i<spectrum1[chanindex]->GetNSubSegmentsMax(0)+spectrum1[chanindex]->GetNSubSegmentsMax(1); i++){
    for(unsigned int j=0; j<G1[i]->GetN(); j++)
      G2[i]->GetY()[j] *= (G1[i]->GetY()[j]/factor);
    delete G1[i];
  }
  delete G1;

  // cosmetics
  tile->SetLogx(1);
  tile->SetLogy(1);
  tile->SetGridx(1);
  tile->SetGridy(1);
  GAPSD2->GetXaxis()->SetMoreLogLabels();
  GAPSD2->SetLineWidth(1);
  GAPSD2->GetXaxis()->SetTitleOffset(0.9);
  GAPSD2->GetYaxis()->SetTitleOffset(1.03);
  GAPSD2->GetXaxis()->SetLabelSize(0.045);
  GAPSD2->GetYaxis()->SetLabelSize(0.045);
  GAPSD2->GetXaxis()->SetTitleSize(0.045);
  GAPSD2->GetYaxis()->SetTitleSize(0.045);
  GAPSD2->GetYaxis()->SetRangeUser(GAPSD2->GetYaxis()->GetXmin()/10.0,
                                   GAPSD2->GetYaxis()->GetXmax()*2.0);
  GAPSD1->SetLineWidth(1);
  if(aType.compare("ASD")){
    GAPSD2->GetHistogram()->SetYTitle("Power [Amp^{2}/Hz]");
    GAPSD2->SetTitle((triggers[chanindex]->GetName()+": Power spectrum density").c_str());
  }
  else{
    GAPSD2->GetHistogram()->SetYTitle("Amplitude [Amp/#sqrt{Hz}]");
    GAPSD2->SetTitle((triggers[chanindex]->GetName()+": Amplitude spectrum density").c_str());
  }

  // draw
  tile->Draw(GAPSD2,"AP");
  for(unsigned int i=0; i<spectrum1[chanindex]->GetNSubSegmentsMax(0)+spectrum1[chanindex]->GetNSubSegmentsMax(1); i++){
    G2[i]->SetLineColor(12);
    tile->Draw(G2[i],"Lsame");
  }
  tile->Draw(GAPSD2,"PLsame");
  tile->RedrawAxis();
  tile->RedrawAxis("g");
  tile->Draw(GAPSD1,"PLsame");

  // set new name
  ss<<aType;
  ss<<"_"<<triggers[chanindex]->GetName()<<"_"<<tile->GetChunkTimeCenter();
  GAPSD2->SetName(ss.str().c_str());
  ss.str(""); ss.clear();

  // ROOT
  if(fOutFormat.find("root")!=string::npos){
    TFile *fpsd;
    ss<<outdir[chanindex]<<"/"+triggers[chanindex]->GetNameConv()<<"_OMICRON"<<aType<<"-"<<tile->GetChunkTimeStart()<<"-"<<tile->GetTimeRange()<<".root";
    fpsd=new TFile((ss.str()).c_str(),"RECREATE");
    ss.str(""); ss.clear();
    fpsd->cd();
    GAPSD2->Write();
    for(unsigned int i=0; i<spectrum1[chanindex]->GetNSubSegmentsMax(0)+spectrum1[chanindex]->GetNSubSegmentsMax(1); i++){
      G2[i]->Write();
    }
    fpsd->Close();
  }

  // Graphix
  vector <string> form;
  if(fOutFormat.find("gif")!=string::npos) form.push_back("gif");
  if(fOutFormat.find("png")!=string::npos) form.push_back("png");
  if(fOutFormat.find("pdf")!=string::npos) form.push_back("pdf");
  if(fOutFormat.find("ps")!=string::npos)  form.push_back("ps");
  if(fOutFormat.find("xml")!=string::npos) form.push_back("xml");
  if(fOutFormat.find("eps")!=string::npos) form.push_back("eps"); 
  if(fOutFormat.find("jpg")!=string::npos) form.push_back("jpg"); 
  if(fOutFormat.find("svg")!=string::npos) form.push_back("svg"); 
  if(form.size()){
    for(unsigned int f=0; f<form.size(); f++){
      ss<<outdir[chanindex]<<"/"+triggers[chanindex]->GetNameConv()<<"_OMICRON"<<aType<<"-"<<tile->GetChunkTimeStart()<<"-"<<tile->GetTimeRange()<<"."<<form[f];
      cout<<ss.str().c_str()<<endl;
      tile->Print(ss.str().c_str());
      ss.str(""); ss.clear();
    }
  }

  // cleaning
  form.clear();
  delete GAPSD1;
  delete GAPSD2;
  for(unsigned int i=0; i<spectrum1[chanindex]->GetNSubSegmentsMax(0)+spectrum1[chanindex]->GetNSubSegmentsMax(1); i++)
    delete G2[i];
  delete G2;

  return;
}

////////////////////////////////////////////////////////////////////////////////////
void Omicron::SaveTS(const bool aWhite){
////////////////////////////////////////////////////////////////////////////////////

  // create graph 
  TGraph *GDATA = new TGraph(offt->GetSize_t());
  stringstream ss;
 
  // whitened data
  if(aWhite){
    ss<<"whitets_"<<triggers[chanindex]->GetNameConv()<<"_"<<tile->GetChunkTimeCenter();
    for(unsigned int i=0; i<offt->GetSize_t(); i++)
      GDATA->SetPoint(i,(double)tile->GetChunkTimeStart()+(double)i/(double)(triggers[chanindex]->GetWorkingFrequency()),offt->GetRe_t(i));
  }
  // conditioned data
  else{
    ss<<"ts_"<<triggers[chanindex]->GetNameConv()<<"_"<<tile->GetChunkTimeCenter();
    for(unsigned int i=0; i<offt->GetSize_t(); i++)
      GDATA->SetPoint(i,(double)tile->GetChunkTimeStart()+(double)i/(double)(triggers[chanindex]->GetWorkingFrequency()),ChunkVect[i]);
  }
 
  // plot name
  GDATA->SetName(ss.str().c_str());
  ss.str(""); ss.clear();
     
  // cosmetics
  tile->SetLogx(0);
  tile->SetLogy(0);
  tile->SetGridx(1);
  tile->SetGridy(1);
  GDATA->GetHistogram()->SetXTitle("Time [s]");
  GDATA->GetHistogram()->SetYTitle("Amplitude");
  if(aWhite)
    GDATA->SetTitle((triggers[chanindex]->GetName()+": amplitude whitened data time series").c_str());
  else
    GDATA->SetTitle((triggers[chanindex]->GetName()+": amplitude conditionned time series").c_str());
  GDATA->SetLineWidth(1);
  GDATA->GetXaxis()->SetNoExponent();
  GDATA->GetXaxis()->SetTitleOffset(1.0);
  GDATA->GetYaxis()->SetTitleOffset(1.03);
  GDATA->GetXaxis()->SetLabelSize(0.045);
  GDATA->GetYaxis()->SetLabelSize(0.045);
  GDATA->GetXaxis()->SetTitleSize(0.045);
  GDATA->GetYaxis()->SetTitleSize(0.045);
  GDATA->GetXaxis()->SetNdivisions(4,5,0);
  tile->Draw(GDATA,"APL");
 
  // ROOT
  if(fOutFormat.find("root")!=string::npos){
    TFile *fdata;
    if(aWhite)
      ss<<outdir[chanindex]<<"/"+triggers[chanindex]->GetNameConv()<<"_OMICRONWHITETS-"<<tile->GetChunkTimeStart()<<"-"<<tile->GetTimeRange()<<".root";
    else
      ss<<outdir[chanindex]<<"/"+triggers[chanindex]->GetNameConv()<<"_OMICRONCONDTS-"<<tile->GetChunkTimeStart()<<"-"<<tile->GetTimeRange()<<".root";
    fdata=new TFile((ss.str()).c_str(),"RECREATE");
    ss.str(""); ss.clear();
    fdata->cd();
    GDATA->Write();
    fdata->Close();
  }

  vector <unsigned int> windows = tile->GetPlotTimeWindows();
 
  // WAV
  if(fOutFormat.find("wav")!=string::npos){
    // adjust volume
    double volume = 0;
    if(GDATA->GetN()) volume=65500.0/GDATA->GetMaximum();

    // loop over windows
    for(int w=(int)windows.size()-1; w>=0; w--){
      if(aWhite)
	ss<<outdir[chanindex]<<"/"+triggers[chanindex]->GetNameConv()<<"_OMICRONWHITETS-"<<tile->GetChunkTimeCenter()<<"-"<<windows[w]<<".wav";
      else
	ss<<outdir[chanindex]<<"/"+triggers[chanindex]->GetNameConv()<<"_OMICRONCONDTS-"<<tile->GetChunkTimeCenter()<<"-"<<windows[w]<<".wav";
      
      // n samples
      unsigned int sound_nstart = TMath::Max((unsigned int)0,(unsigned int)(((double)tile->GetChunkTimeCenter()+toffset-(double)windows[w]/2.0-(double)tile->GetChunkTimeStart())*(double)triggers[chanindex]->GetWorkingFrequency()));
      unsigned int sound_n = TMath::Min(GDATA->GetN()*windows[w]/(tile->GetChunkTimeEnd()-tile->GetChunkTimeStart()), GDATA->GetN()-sound_nstart);

      // generate wav file
      MakeStereoSoundFile(ss.str(),
			  sound_n,
			  triggers[chanindex]->GetWorkingFrequency(), GDATA->GetY(), GDATA->GetY(), volume,
			  sound_nstart);
      ss.str(""); ss.clear();

    }
  }
 
  // Graphix
  vector <string> form;
  if(fOutFormat.find("gif")!=string::npos) form.push_back("gif");
  if(fOutFormat.find("png")!=string::npos) form.push_back("png");
  if(fOutFormat.find("pdf")!=string::npos) form.push_back("pdf");
  if(fOutFormat.find("ps")!=string::npos)  form.push_back("ps");
  if(fOutFormat.find("xml")!=string::npos) form.push_back("xml");
  if(fOutFormat.find("eps")!=string::npos) form.push_back("eps"); 
  if(fOutFormat.find("jpg")!=string::npos) form.push_back("jpg"); 
  if(fOutFormat.find("svg")!=string::npos) form.push_back("svg"); 
  if(form.size()){
    
    // zoom
    for(int w=(int)windows.size()-1; w>=0; w--){
      GDATA->GetXaxis()->SetLimits(tile->GetChunkTimeCenter()+toffset-(double)windows[w]/2.0,tile->GetChunkTimeCenter()+toffset+(double)windows[w]/2.0);
      for(unsigned int f=0; f<form.size(); f++){
	if(aWhite)
	  ss<<outdir[chanindex]<<"/"+triggers[chanindex]->GetNameConv()<<"_OMICRONWHITETS-"<<tile->GetChunkTimeCenter()<<"-"<<windows[w]<<"."<<form[f];
	else
	  ss<<outdir[chanindex]<<"/"+triggers[chanindex]->GetNameConv()<<"_OMICRONCONDTS-"<<tile->GetChunkTimeCenter()<<"-"<<windows[w]<<"."<<form[f];
	tile->Print(ss.str().c_str());
	ss.str(""); ss.clear();

	if(aWhite)
	  ss<<outdir[chanindex]<<"/th"+triggers[chanindex]->GetNameConv()<<"_OMICRONWHITETS-"<<tile->GetChunkTimeCenter()<<"-"<<windows[w]<<"."<<form[f];
	else
	  ss<<outdir[chanindex]<<"/th"+triggers[chanindex]->GetNameConv()<<"_OMICRONCONDTS-"<<tile->GetChunkTimeCenter()<<"-"<<windows[w]<<"."<<form[f];
	tile->Print(ss.str().c_str(),0.5);
	ss.str(""); ss.clear();
      }
    }
  }
      
  form.clear();
  delete GDATA;
  return;
}

////////////////////////////////////////////////////////////////////////////////////
void Omicron::SaveWPSD(void){
////////////////////////////////////////////////////////////////////////////////////

  // extract PSD
  TGraph *GPSD = spectrumw->GetPSD(tile->GetFrequencyMin(),tile->GetFrequencyMax());
  
  // cosmetics
  tile->SetLogx(1);
  tile->SetLogy(1);
  tile->SetGridx(1);
  tile->SetGridy(1);
  tile->Draw(GPSD,"APL");
  tile->RedrawAxis();
  tile->RedrawAxis("g");
  GPSD->SetLineWidth(1);
  GPSD->GetXaxis()->SetMoreLogLabels();
  GPSD->GetXaxis()->SetTitleOffset(0.9);
  GPSD->GetYaxis()->SetTitleOffset(1.03);
  GPSD->GetXaxis()->SetLabelSize(0.045);
  GPSD->GetYaxis()->SetLabelSize(0.045);
  GPSD->GetXaxis()->SetTitleSize(0.045);
  GPSD->GetYaxis()->SetTitleSize(0.045);
  GPSD->GetHistogram()->SetXTitle("Frequency [Hz]");
  GPSD->GetHistogram()->SetYTitle("Power [Amp^{2}/Hz]");
  GPSD->SetTitle((triggers[chanindex]->GetName()+": Power spectrum density").c_str());

  // set new name
  stringstream ss;
  ss<<"PSDW_"<<triggers[chanindex]->GetNameConv()<<"_"<<tile->GetChunkTimeCenter();
  GPSD->SetName(ss.str().c_str());
  ss.str(""); ss.clear();

  // ROOT
  if(fOutFormat.find("root")!=string::npos){
    TFile *fpsd;
    ss<<outdir[chanindex]<<"/"+triggers[chanindex]->GetNameConv()<<"_OMICRONWPSD-"<<tile->GetChunkTimeStart()<<"-"<<tile->GetTimeRange()<<".root";
    fpsd=new TFile((ss.str()).c_str(),"RECREATE");
    ss.str(""); ss.clear();
    fpsd->cd();
    GPSD->Write();
    fpsd->Close();
  }

  // Graphix
  vector <string> form;
  if(fOutFormat.find("gif")!=string::npos) form.push_back("gif");
  if(fOutFormat.find("png")!=string::npos) form.push_back("png");
  if(fOutFormat.find("pdf")!=string::npos) form.push_back("pdf");
  if(fOutFormat.find("ps")!=string::npos)  form.push_back("ps");
  if(fOutFormat.find("xml")!=string::npos) form.push_back("xml");
  if(fOutFormat.find("eps")!=string::npos) form.push_back("eps"); 
  if(fOutFormat.find("jpg")!=string::npos) form.push_back("jpg"); 
  if(fOutFormat.find("svg")!=string::npos) form.push_back("svg"); 
  if(form.size()){
    for(unsigned int f=0; f<form.size(); f++){
      ss<<outdir[chanindex]<<"/"+triggers[chanindex]->GetNameConv()<<"_OMICRONWPSD-"<<tile->GetChunkTimeStart()<<"-"<<tile->GetTimeRange()<<"."<<form[f];
      tile->Print(ss.str().c_str());
      ss.str(""); ss.clear();
    }
  }
  
  form.clear();
  delete GPSD;

  return;
}
