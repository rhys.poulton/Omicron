/**
 * @file 
 * @brief See Oqplane.h
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#include "Oqplane.h"

ClassImp(Oqplane)

////////////////////////////////////////////////////////////////////////////////////
Oqplane::Oqplane(const double aQ, const unsigned int aSampleFrequency,
		 const double aFrequencyMin, const double aFrequencyMax, 
		 const unsigned int aTimeRange, const double aMaximumMismatch):
Omap(aQ, aSampleFrequency, aFrequencyMin, aFrequencyMax, aTimeRange, aMaximumMismatch){ 
////////////////////////////////////////////////////////////////////////////////////
  
  // default SNR threshold
  SetSnrThr();

  // band filtering
  bandNoiseAmplitude = new double       [Omap::GetBandN()];
  bandFFT            = new fft*         [Omap::GetBandN()];
  bandWindow_r       = new double*      [Omap::GetBandN()];
  bandWindow_i       = new double*      [Omap::GetBandN()];
  bandWindowSize     = new unsigned int [Omap::GetBandN()];

  double bandfrequency;
  double windowargument;
  double winnormalization;
  double ifftnormalization;
  double delta_f;// Connes window 1/2-width
  int k, end;
     
  for(unsigned int f=0; f<Omap::GetBandN(); f++){
    
    // no power
    bandNoiseAmplitude[f]=0.0;
            
    // band fft
    ifftnormalization = 1.0 / (double)Omap::GetTimeRange();
    bandFFT[f] = new fft(Omap::GetBandTileN(f),"FFTW_ESTIMATE", "c2c");

    // Prepare window stuff
    bandfrequency = Omap::GetBandFrequency(f);
    delta_f = bandfrequency/(Omap::GetQ()/TMath::Sqrt(11.0));// from eq. 5.18
    bandWindowSize[f] = 2*(unsigned int)floor(delta_f*(double)Omap::GetTimeRange()) + 1;
    bandWindow_r[f]   = new double [bandWindowSize[f]];
    bandWindow_i[f]   = new double [bandWindowSize[f]];
    winnormalization  = TMath::Sqrt(315.0*Omap::GetQ()/TMath::Sqrt(11.0)/128.0/bandfrequency);// eq. 5.26 Localized bursts only!!!

    // bisquare window
    end=(bandWindowSize[f]+1)/2;
    for(k=0; k<end; k++){
      windowargument=2.0*(double)k/(double)(bandWindowSize[f] - 1);
      bandWindow_r[f][k] = winnormalization*ifftnormalization*(1.0-windowargument*windowargument)*(1.0-windowargument*windowargument)*TMath::Cos(TMath::Pi()*(double)k/(double)Omap::GetBandTileN(f));// bisquare window (1-x^2)^2 and phase shift
      bandWindow_i[f][k] = winnormalization*ifftnormalization*(1.0-windowargument*windowargument)*(1.0-windowargument*windowargument)*TMath::Sin(TMath::Pi()*(double)k/(double)Omap::GetBandTileN(f));// bisquare window (1-x^2)^2 and phase shift
    }
    // do not save 0s in the center
    end=bandWindowSize[f];
    for(; k<end; k++){
      windowargument=2.0*(double)(k-end)/(double)(bandWindowSize[f] - 1);
      bandWindow_r[f][k] = -winnormalization*ifftnormalization*(1.0-windowargument*windowargument)*(1.0-windowargument*windowargument)*TMath::Cos(TMath::Pi()*(double)(k-(int)bandWindowSize[f]+(int)Omap::GetBandTileN(f))/(double)Omap::GetBandTileN(f));// bisquare window (1-x^2)^2 and phase shift
      bandWindow_i[f][k] = -winnormalization*ifftnormalization*(1.0-windowargument*windowargument)*(1.0-windowargument*windowargument)*TMath::Sin(TMath::Pi()*(double)(k-(int)bandWindowSize[f]+(int)Omap::GetBandTileN(f))/(double)Omap::GetBandTileN(f));// bisquare window (1-x^2)^2 and phase shift
    }
  }
  
}

////////////////////////////////////////////////////////////////////////////////////
Oqplane::~Oqplane(void){
////////////////////////////////////////////////////////////////////////////////////
  for(int f=0; f<Omap::GetBandN(); f++){
    delete bandFFT[f];
    delete bandWindow_r[f];
    delete bandWindow_i[f];
  }
  delete bandWindowSize;
  delete bandFFT;
  delete bandNoiseAmplitude;
  delete bandWindow_r;
  delete bandWindow_i;
}

////////////////////////////////////////////////////////////////////////////////////
long unsigned int Oqplane::ProjectData(fft *aDataFft, const double aPadding){
////////////////////////////////////////////////////////////////////////////////////

  // locals
  int k, Pql, Nt, end;
  double snrthr2=SnrThr*SnrThr;
  long unsigned int nt=0;
  unsigned int tstart, tend;
  
  // loop over frequency bands
  for(unsigned int f=0; f<Omap::GetBandN(); f++){
    
    // number of tiles in this band
    Nt = (int)Omap::GetBandTileN(f);
    
    // frequency index shift for this band
    Pql=(int)floor(Omap::GetBandFrequency(f)*(double)Omap::GetTimeRange());
    
    // populate \tilde{v}
    end=(bandWindowSize[f]+1)/2;
    for(k=0; k<end; k++){
      bandFFT[f]->SetRe_f((unsigned int)k,
                          bandWindow_r[f][k]*aDataFft->GetRe_f((unsigned int)(k+Pql))
                          - bandWindow_i[f][k]*aDataFft->GetIm_f((unsigned int)(k+Pql)));
      bandFFT[f]->SetIm_f((unsigned int)k,
                          bandWindow_i[f][k]*aDataFft->GetRe_f((unsigned int)(k+Pql))
                          + bandWindow_r[f][k]*aDataFft->GetIm_f((unsigned int)(k+Pql)));
    }
    end=Nt-(bandWindowSize[f]-1)/2;
    for(; k<end; k++){
      bandFFT[f]->SetRe_f((unsigned int)k, 0.0);
      bandFFT[f]->SetIm_f((unsigned int)k, 0.0);
    }
    end=Nt;
    for(; k<end; k++){
      bandFFT[f]->SetRe_f((unsigned int)k,
                          bandWindow_r[f][k-Nt+(int)bandWindowSize[f]]*(aDataFft->GetRe_f((unsigned int)abs(Nt-k-Pql))) - bandWindow_i[f][k-Nt+(int)bandWindowSize[f]]*(aDataFft->GetIm_f((unsigned int)abs(Nt-k-Pql))));
      bandFFT[f]->SetIm_f((unsigned int)k,
                          bandWindow_r[f][k-Nt+(int)bandWindowSize[f]]*(aDataFft->GetIm_f((unsigned int)abs(Nt-k-Pql))) + bandWindow_i[f][k-Nt+(int)bandWindowSize[f]]*(aDataFft->GetRe_f((unsigned int)abs(Nt-k-Pql))));
    }

    // fft-backward
    bandFFT[f]->Backward();
    // note the FFT normalization was already included in the window definition
    
    // from now on, the final Q coefficients are stored in the time vector of bandFFT[f]
    
    // count tiles above threshold
    tstart=Omap::GetTimeTileIndex(f, Omap::GetTimeMin()+aPadding);
    tend=Omap::GetTimeTileIndex(f, Omap::GetTimeMax()-aPadding);
    for(unsigned int t=tstart; t<tend; t++){
      if(GetTileSnrSq(t,f)>=snrthr2) nt++;
    }
  }
 
  return nt;
}

////////////////////////////////////////////////////////////////////////////////////
void Oqplane::FillMap(const string aContentType, const double aTimeStart, const double aTimeEnd){
////////////////////////////////////////////////////////////////////////////////////

  // start and end time
  unsigned int tstart, tend;
  if(!aContentType.compare("snr")){
    for(unsigned int f=0; f<Omap::GetBandN(); f++){
      tstart=Omap::GetTimeTileIndex(f,aTimeStart);
      tend=Omap::GetTimeTileIndex(f,aTimeEnd);
      for(unsigned int t=tstart; t<=tend; t++)
	SetTileContent(t,f,TMath::Sqrt(GetTileSnrSq(t,f)));
    }
  }
  else if(!aContentType.compare("amplitude")){
    for(unsigned int f=0; f<Omap::GetBandN(); f++){
      tstart=Omap::GetTimeTileIndex(f,aTimeStart);
      tend=Omap::GetTimeTileIndex(f,aTimeEnd);
      for(unsigned int t=tstart; t<=tend; t++)
	SetTileContent(t,f,GetTileAmplitude(t,f));
    }
  }
  else if(!aContentType.compare("phase")){
    for(unsigned int f=0; f<Omap::GetBandN(); f++){
      tstart=Omap::GetTimeTileIndex(f,aTimeStart);
      tend=Omap::GetTimeTileIndex(f,aTimeEnd);
      for(unsigned int t=tstart; t<=tend; t++)
	SetTileContent(t,f,bandFFT[f]->GetPhase_t(t));
    }
  }
  else{
    for(unsigned int f=0; f<Omap::GetBandN(); f++){
      tstart=Omap::GetTimeTileIndex(f,aTimeStart);
      tend=Omap::GetTimeTileIndex(f,aTimeEnd);
      for(unsigned int t=tstart; t<=tend; t++)
	SetTileContent(t,f,t%2);
    }
  }

  // set Z-axis title
  tfmap->GetZaxis()->SetTitle(StringToUpper(aContentType).c_str());
  
  return;
}

////////////////////////////////////////////////////////////////////////////////////
void Oqplane::AddTileSegments(Segments *aSegments, TH1D *aSnrThreshold,
                              const double aT0, const double aPadding){
////////////////////////////////////////////////////////////////////////////////////

  if(aSnrThreshold==NULL) return;
  if(aSegments==NULL) return;
  if(aSnrThreshold->GetNbinsX()<=0) return;

  unsigned int tstart, tend;
  double snr2, snr2thr, frequency;
  int binnb;
  
  // loop over frequency bands
  for(unsigned int f=0; f<Omap::GetBandN(); f++){
    
    // band frequency
    frequency = Omap::GetBandFrequency(f);
    if(frequency<aSnrThreshold->GetXaxis()->GetBinLowEdge(1)) continue;
    if(frequency>=aSnrThreshold->GetXaxis()->GetBinUpEdge(aSnrThreshold->GetNbinsX())) break;

    // corresponding bin number
    binnb = aSnrThreshold->GetXaxis()->FindBin(frequency);

    // infinite threshold --> skip frequency row
    if(aSnrThreshold->GetBinContent(binnb)<0) continue;

    // SNRsq threshold
    snr2thr = aSnrThreshold->GetBinContent(binnb) * aSnrThreshold->GetBinContent(binnb);
    
    // time range
    tstart=Omap::GetTimeTileIndex(f, Omap::GetTimeMin()+aPadding);
    tend=Omap::GetTimeTileIndex(f, Omap::GetTimeMax()-aPadding);

    // add segments
    for(unsigned int t=tstart; t<tend; t++){

      // get tile snr2
      snr2=GetTileSnrSq(t,f);

      // apply SNR threshold
      if(snr2<snr2thr) continue;

      // new segment
      aSegments->AddSegment(Omap::GetTileTimeStart(t,f)+aT0, Omap::GetTileTimeEnd(t,f)+aT0);
    }
  }

  return;
}

////////////////////////////////////////////////////////////////////////////////////
bool Oqplane::SaveTriggers(TriggerBuffer *aTriggers, const double aT0, Segments* aSeg){
////////////////////////////////////////////////////////////////////////////////////
  unsigned int tstart, tend;
  double snr2, snr, frequency, frequency_s, frequency_e;
  double SnrThr2 = SnrThr*SnrThr;

  // no segments
  if(aSeg->GetN()<1) return true;
  
  for(unsigned int f=0; f<Omap::GetBandN(); f++){
    
    // remove padding
    tstart=(unsigned int)TMath::Max(Omap::GetTimeTileIndex(f, aSeg->GetFirst()-aT0), 0);
    tend=(unsigned int)TMath::Min((unsigned int)Omap::GetTimeTileIndex(f, aSeg->GetLast()-aT0)+1, GetBandTileN(f));

    // band frequency
    frequency = Omap::GetBandFrequency(f);
    frequency_s = Omap::GetBandStart(f);
    frequency_e = Omap::GetBandEnd(f);

    // fill triggers
    for(unsigned int t=tstart; t<tend; t++){

      // get tile snr2
      snr2=GetTileSnrSq(t,f);

      // apply SNR threshold
      if(snr2<SnrThr2) continue;

      // time selection
      if(!aSeg->IsInsideSegment(Omap::GetTileTime(t,f)+aT0)) continue;

      // amplitude SNR
      snr=TMath::Sqrt(snr2);
      
      // save trigger
      if(!aTriggers->AddTrigger(Omap::GetTileTime(t,f)+aT0,
				frequency,
				snr,
				Omap::GetQ(),
				Omap::GetTileTimeStart(t,f)+aT0,
				Omap::GetTileTimeEnd(t,f)+aT0,
				frequency_s,
				frequency_e,
				snr*bandNoiseAmplitude[f],
				bandFFT[f]->GetPhase_t(t)))
	return false;
    }
  }

  // for trigger segments see Otile::SaveTriggers()
  return true;
}

////////////////////////////////////////////////////////////////////////////////////
void Oqplane::SetPower(Spectrum *aSpec1, Spectrum *aSpec2){
////////////////////////////////////////////////////////////////////////////////////

  //double sumofweight;
  double sum, Wb, deltaf, bandfreq, freq, win;
  unsigned int i, end;
  
  double dfreq = 1.0 / (double)Omap::GetTimeRange();

  // set power for each f-row
  for(unsigned int f=0; f<Omap::GetBandN(); f++){

    sum=0;
    bandfreq = Omap::GetBandFrequency(f);
    Wb = TMath::Sqrt(315.0/128.0*Omap::GetQ()/TMath::Sqrt(11.0)/bandfreq);
    deltaf = bandfreq*TMath::Sqrt(11.0)/Omap::GetQ();
    i=0;

    // f > phi
    end=(bandWindowSize[f]+1)/2;
    for(; i<end; i++){
      freq = bandfreq + (double)i/(double)Omap::GetTimeRange();
      win = (1.0-(freq-bandfreq)*(freq-bandfreq)/deltaf/deltaf);
      win *= (Wb*win);
      sum += win*win /2.0 /TMath::Sqrt(aSpec1->GetPower(freq)*aSpec2->GetPower(freq)/4.0) * dfreq;
    }

    // f < phi
    end=bandWindowSize[f];
    for(; i<end; i++){
      freq=bandfreq-(double)(end-i)/(double)Omap::GetTimeRange();
      win = (1.0-(freq-bandfreq)*(freq-bandfreq)/deltaf/deltaf);
      win *= (Wb*win);
      sum += win*win /2.0 /TMath::Sqrt(aSpec1->GetPower(freq)*aSpec2->GetPower(freq)/4.0) * dfreq;
    }
        
    bandNoiseAmplitude[f]=1.0/sum;
  }
  
  return;
}
