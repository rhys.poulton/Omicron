/**
 * @file 
 * @brief See Ox.h
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#include "Ox.h"

ClassImp(Ox)

////////////////////////////////////////////////////////////////////////////////////
Ox::Ox(const string aOptionFile1, const string aOptionFile2){
////////////////////////////////////////////////////////////////////////////////////

  // Ox default parameters
  flag_full_only = true;
  flag_normalize = true;

  // omicron objects
  O1 = new Omicron(aOptionFile1);
  O2 = new Omicron(aOptionFile2);
  status = (O1->GetStatus())&&(O2->GetStatus());

  // there must be only one channel
  if(O1->GetChannelsN() != 1){
    cerr<<"Ox::Ox: only one channel can be processed"<<endl;
    status = false;
  }
  if(O2->GetChannelsN() != 1){
    cerr<<"Ox::Ox: only one channel can be processed"<<endl;
    status = false;
  }

  // check that the tiling is the same
  if(O1->GetChunkDuration() != O2->GetChunkDuration()){
    cerr<<"Ox::Ox: analysis window mismatch"<<endl;
    status = false;
  }
  if(O1->GetOverlapDuration() != O2->GetOverlapDuration()){
    cerr<<"Ox::Ox: window overlap mismatch"<<endl;
    status = false;
  } 
  if(O1->GetQN() != O2->GetQN()){
    cerr<<"Ox::Ox: number of Q planes mismatch"<<endl;
    status = false;
  }
  else{
    for(unsigned int q=0; q<O1->GetQN(); q++){
      if(O1->GetBandN(q) != O2->GetBandN(q)){
        cerr<<"Ox::Ox: number of frequency rows mismatch"<<endl;
        status = false;
        break;
      }
      // we do not check tiles in a row as this should be automatically OK
    }
  }

  // FFT plan: use the maximum resolution
  unsigned int win_size = (O1->GetChunkDuration() - O1->GetOverlapDuration())*O1->GetBandTileN(0, O1->GetBandN(0)-1);
  win_size /= O1->GetChunkDuration();
  fft1 = new fft(win_size, "FFTW_ESTIMATE", "r2c");
  fft2 = new fft(win_size, "FFTW_ESTIMATE", "r2c");

  // histograms
  stringstream ss1, ss2;
  double *bins;
  hx = new TH2D* [O1->GetQN()+1]; // +1 = full map
  h1 = new TH2D* [O1->GetQN()+1];
  h2 = new TH2D* [O1->GetQN()+1];
  for(unsigned int q=0; q<=O1->GetQN(); q++){
    bins = O1->GetBands(q);
    ss1<<"q"<<q;
    ss2<<"Q="<<O1->GetQ(q);
    h1[q] = new TH2D(("h1_"+ss1.str()).c_str(), ("h1 ("+ss2.str()+")").c_str(), fft1->GetSize_t(),
                     0.0, (double)(O1->GetChunkDuration()-O1->GetOverlapDuration()),
                     O1->GetBandN(q), bins);
    h2[q] = new TH2D(("h2_"+ss1.str()).c_str(), ("h2 ("+ss2.str()+")").c_str(), fft1->GetSize_t(),
                     0.0, (double)(O1->GetChunkDuration()-O1->GetOverlapDuration()),
                     O1->GetBandN(q), bins);
    hx[q] = new TH2D(("xi_"+ss1.str()).c_str(), ("#xi(t,f) ("+ss2.str()+")").c_str(), fft1->GetSize_t(),
                     -(double)(O1->GetChunkDuration()-O1->GetOverlapDuration())/2.0, (double)(O1->GetChunkDuration()-O1->GetOverlapDuration())/2.0,
                     O1->GetBandN(q), bins);
    ss1.clear(); ss1.str("");
    ss2.clear(); ss2.str("");
    delete bins;
  }

}

////////////////////////////////////////////////////////////////////////////////////
Ox::~Ox(void){
////////////////////////////////////////////////////////////////////////////////////
  for(unsigned int q=0; q<=O1->GetQN(); q++){
    delete h1[q];
    delete h2[q];
    delete hx[q];
  }
  delete h1;
  delete h2;
  delete hx;
  delete fft1;
  delete fft2;
  delete O1;
  delete O2;
}
 
////////////////////////////////////////////////////////////////////////////////////
bool Ox::Process(Segments *aInSeg){
////////////////////////////////////////////////////////////////////////////////////
  if(!status){
    cerr<<"Ox::Process: the Ox object is corrupted"<<endl;
    return false;
  }

  bool reset_psd;
  unsigned int gstart;
  unsigned int datasize1, datasize2;
  double *data1, *data2;
  double median;
  unsigned int t_start, t_resample;
  bool is_flat;// not used
  double norm, realpart;
  TFile *fout;
  stringstream ss;
  TH2D *h1_full, *h2_full;

  // loop over segments
  for(unsigned int s=0; s<aInSeg->GetN(); s++){

    gstart = aInSeg->GetStart(s);
    reset_psd = true;
    
    // loop over analysis windows
    while(gstart + O1->GetChunkDuration() <= (unsigned int)aInSeg->GetEnd(s)){
      cout<<gstart<<"..."<<endl;
      // define analysis chunk
      if(!O1->DefineNewChunk(gstart, gstart + O1->GetChunkDuration(), reset_psd)) break;
      if(!O2->DefineNewChunk(gstart, gstart + O1->GetChunkDuration(), reset_psd)) break;
      reset_psd = false;

      // load channel
      if(O1->NewChannel()==false) break;
      if(O2->NewChannel()==false) break;

      // load data
      if(O1->LoadData(&data1, datasize1)==false) break;
      if(O2->LoadData(&data2, datasize2)==false){ delete data1; break; }

      // condition data
      if(O1->Condition(datasize1, data1, is_flat)!=0){
        delete data1;
        delete data2;
        break;
      }
      if(O2->Condition(datasize2, data2, is_flat)!=0){
        delete data1;
        delete data2;
        break;
      }
      delete data1;
      delete data2;

      // project data
      O1->Project();
      O2->Project();

      // get full maps
      O1->FillMaps();
      O2->FillMaps();
      h1_full = O1->GetFullMap();
      h2_full = O2->GetFullMap();

      // output file
      ss<<"./omicronx-"<<gstart+O1->GetOverlapDuration()/2<<"-"<<O1->GetChunkDuration()-O1->GetOverlapDuration()<<".root";
      fout = new TFile(ss.str().c_str(), "recreate");
      ss.clear(); ss.str("");
      
      // cross-correlation of individual Q planes and full map (last index)
      for(unsigned int q=0; q<=O1->GetQN(); q++){

        // do not process individual Q planes
        if((flag_full_only == true) && q<O1->GetQN()) continue;
               
        // loop over frequency rows
        for(unsigned int f=0; f<O1->GetBandN(q); f++){
          
          // start after overlap/2
          t_start = (O1->GetOverlapDuration()/2)*O1->GetBandTileN(q,f);
          t_start /= O1->GetChunkDuration();
          
          // fill time-domain vectors
          for(unsigned int t=0; t<fft1->GetSize_t(); t++){
            
            // down-sample index
            t_resample = t*(O1->GetBandTileN(q,f)-2*t_start)/fft1->GetSize_t();

            // fill frequency row
            if(q<O1->GetQN()){// individual Q plane
              h1[q]->SetBinContent(t+1, f+1, O1->GetTileSnrSq(q,f,t_start+t_resample));
              h2[q]->SetBinContent(t+1, f+1, O2->GetTileSnrSq(q,f,t_start+t_resample));
            }
            else{// full map
              h1[q]->SetBinContent(t+1, f+1, h1_full->GetBinContent(t_start+t_resample+1, f+1));
              h2[q]->SetBinContent(t+1, f+1, h2_full->GetBinContent(t_start+t_resample+1, f+1));
            }
            fft1->SetRe_t(t, h1[q]->GetBinContent(t+1, f+1)/(double)fft1->GetSize_t());
            fft2->SetRe_t(t, h2[q]->GetBinContent(t+1, f+1)/(double)fft1->GetSize_t());
          }
          
          // fft forward
          fft1->Forward();
          fft2->Forward();
          
          // cross-correlation in the fourier domain
          for(unsigned int k=0; k<fft1->GetSize_f(); k++){
            
            // norm + phase factors
            if(k%2==1) norm = -(double)fft1->GetSize_t();
            else       norm = (double)fft1->GetSize_t();
            
            realpart = fft1->GetRe_f(k);
            fft1->SetRe_f(k, norm*(realpart*fft2->GetRe_f(k) + fft1->GetIm_f(k)*fft2->GetIm_f(k)));
            fft1->SetIm_f(k, norm*(fft1->GetIm_f(k)*fft2->GetRe_f(k) - realpart*fft2->GetIm_f(k)));
          }
          
          // fft backward
          fft1->Backward();
          
          // get cross-correlation median
          if(flag_normalize == true){
            data1 = fft1->GetRe_t();
            median = GetMedian(fft1->GetSize_t(), data1);
            delete data1;
          }
          else median = 0.0;
          
          // fill cross-correlation histogram
          for(unsigned int t=0; t<fft1->GetSize_t(); t++){
            hx[q]->SetBinContent(t+1, f+1, fft1->GetRe_t(t)-median);
          }
        }
        
        // save histo
        fout->cd();
        h1[q]->Write();
        h2[q]->Write();
        hx[q]->Write();
      }

      delete h1_full;
      delete h2_full;

      // next chunk (50% overlap)
      gstart += O1->GetChunkDuration()/2 - O1->GetOverlapDuration()/2;
      while(O1->NewChannel());
      while(O2->NewChannel());

      // close output file
      if(fout->IsOpen()) fout->Close();
    }

  }

  return true;
}
